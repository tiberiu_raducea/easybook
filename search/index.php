<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Search Results Listing</h4>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Metus dictum at tempor commodo ullamcorper a lacus vestibulum
                            sed. Nibh mauris cursus mattis molestie a iaculis.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_search_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell large-shrink">
                <form action="" class="eb_filter_search_results"></form>
                <!-- "processing" class on the following will replicate a loading status-->
                <div class="eb_search_filters" id="ebSearchFilters" data-toggler="active">

                    <div class="header">Filter
                        <button class="eb_search_filters_toggler" role="button" data-toggle="ebSearchFilters faToggle">
                            <span class="fa fa-plus" id="faToggle" data-toggler="fa-plus fa-minus"></span></button>
                    </div>
                    <div class="body">
                        <div class="eb_filter_block">
                            <div class="eb_filter_title">Distance in miles</div>
                            <div class="eb_filters">
                                <label for="filter_1_block_1">
                                    <input type="radio" name="distance_in_miles" id="filter_1_block_1">
                                    <span>30 Miles</span>
                                </label>

                                <label for="filter_2_block_1">
                                    <input type="radio" name="distance_in_miles" id="filter_2_block_1">
                                    <span>60 Miles</span>
                                </label>

                                <label for="filter_3_block_1">
                                    <input type="radio" name="distance_in_miles" id="filter_3_block_1">
                                    <span>All UK</span>
                                </label>
                            </div>
                        </div>
                        <div class="eb_filter_block">
                            <div class="eb_filter_title">Delivery schedule</div>
                            <div class="eb_filters">
                                <label for="filter_1_block_2">
                                    <input type="checkbox" id="filter_1_block_2">
                                    <span>Block courses</span>
                                </label>

                                <label for="filter_2_block_2">
                                    <input type="checkbox" id="filter_2_block_2">
                                    <span>Day Release</span>
                                </label>

                                <label for="filter_3_block_2">
                                    <input type="checkbox" id="filter_3_block_2">
                                    <span>Weekend</span>
                                </label>
                            </div>
                        </div>
                        <div class="eb_filter_block">
                            <div class="eb_filter_title">CITB Grant</div>
                            <div class="eb_filters">
                                <label for="filter_1_block_3">
                                    <input type="checkbox" id="filter_1_block_3">
                                    <span>Eligible for CITB Grant</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <button class="button eb_btn shrunk expanded">Update results</button>
                    </div>
                </div>
            </div>
            <div class="cell large-auto">
                <div class="grid-x grid-padding-x align-justify align-middle">
                    <div class="cell large-shrink">
                        <div class="table_info"><span class="fa fa-envelope"></span>Save & Send <i>?</i></div>
                        <div class="table_info"><span class="fa fa-g">G</span> Identifies courses which are eligible for
                            the CITB Grant
                        </div>
                    </div>
                    <div class="cell large-shrink">
                        <div class="global_table_cta">
                            <a href=""><span class="fa fa-envelope"></span> 2 Courses</a>
                            <a href="" class="button eb_btn small">email list</a>
                            <a href="" class="button eb_btn dark small">clear list</a>
                        </div>
                    </div>
                </div>
                <?php for ($x = 0; $x < 3; $x++): ?>
                    <div class="eb_search_table">
                        <div class="eb_search_table_header">
                            <div class="eb_search_table_row">
                                <div class="eb_search_table_data"><span class="name">18 Stree Name, County (City) <span>xx.x miles away</span></span></div>
                                <div class="eb_search_table_data large-text-right"><a href="#" class="table_head_link">View
                                        on map</a></div>
                            </div>

                        </div>
                        <div class="eb_search_table_body">
                            <div class="eb_search_table_row defined">
                                <div class="eb_search_table_data"><span class="table_sub_title">Course</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Start Date</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">End Date</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Delivery</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Price <sup>exVAT</sup></span></div>
                                <div class="eb_search_table_data"></div>
                                <div class="eb_search_table_data"></div>
                            </div>
                            <?php for ($i = 0; $i < 7; $i++) : ?>
                                <div class="eb_search_table_row defined <?php if ($i == 3): ?>item_on_sale<?php endif; ?>">
                                    <div class="eb_search_table_data">
                                        <a href="#" title="CITB SSSTS <?php echo $i ?> Day Course" class="name">CITB SSSTS <?php echo $i ?> Day Course Longer title here <?php if ($i == 3): ?>
                                                <sup>sale</sup><?php endif; ?></a></div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">Sat, 0<?php echo $i + 1 ?> Jan 10</span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">Mon, 31 Dec 10</span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">Block</span></div>
                                    <div class="eb_search_table_data">
                                        <span class="item_price <?php if ($i == 3): ?>reduced<?php endif; ?>"><span
                                                    class="site_currency">&pound;</span>250 <?php if ($i == 3): ?><sup>&pound;350</sup><?php endif; ?> </span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <div class="item_cta equalized">
                                            <?php if ($i == 2) : ?><a href="" class="button eb_btn shrunk light"><span class="breakpoint">please</span>
                                                call</a><?php elseif ($i == 5) : ?><a href=""
                                                                                      class="button eb_btn shrunk dark">call
                                                <span class="breakpoint">to book</span></a><?php else: ?><a href="" class="button eb_btn shrunk">view
                                                <span class="breakpoint">details</span></a><?php endif; ?>
                                            <a href="#"><span class="fa fa-envelope"></span></a>
                                        </div>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <div class="item_cta"><?php if (!($i % 3)) : ?><span
                                                    class="fa fa-g">G</span><?php endif; ?> <?php if (!($i % 6)) : ?><a
                                                    href=""
                                                    class="button eb_btn shrunk">Book</a><?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <div class="eb_search_table_footer">
                            <div class="eb_search_table_row">
                                <div class="eb_search_table_data">
                                    <a href=""><span class="fa fa-plus"></span> view more courses</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>

                <div class="grid-x grid-padding-x">
                    <div class="cell small-12 text-center">
                        <nav aria-label="Pagination" class="">
                            <ul class="pagination">
                                <!-- <li class="pagination-previous disabled">Previous <span class="show-for-sr">page</span></li> -->
                                <!-- <li class="pagination-previous"><a href="#" aria-label="Previous page">Previous <span class="show-for-sr">page</span></a></li> -->
                                <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                                <li><a href="#" aria-label="Page 2">2</a></li>
                                <li><a href="#" aria-label="Page 3">3</a></li>
                                <li><a href="#" aria-label="Page 4">4</a></li>
                                <li><a href="#" aria-label="Page 5">5</a></li>
                                <li><a href="#" aria-label="Page 6">6</a></li>
                                <li class="pagination-next"><a href="#" aria-label="Next page">Next <span
                                                class="show-for-sr">page</span></a></li>
                            </ul>
                        </nav>
                    </div>

                    <div class="cell small-12 text-center">
                        <div class="eb_cms_content formatted up-down just-down">
                            <h4>Lorem ipsum dolor sit amet, consectetur adipiscing</h4>

                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Metus dictum at tempor commodo ullamcorper a lacus
                                vestibulum sed. Nibh mauris cursus mattis molestie a iaculis.</p>

                            <p>Tortor consequat id porta nibh venenatis. Erat pellentesque adipiscing commodo elit at
                                imperdiet dui accumsan sit. A arcu cursus vitae congue mauris rhoncus. Nascetur
                                ridiculus mus mauris vitae ultricies. Integer eget aliquet nibh praesent tristique magna
                                sit amet purus. Ultrices vitae auctor eu augue ut lectus arcu bibendum. Nunc eget lorem
                                dolor sed viverra ipsum nunc aliquet.</p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>


<?php include('../template/footer.php'); ?>
