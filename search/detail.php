<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area up-down small just-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="grid-x grid-padding-x">
                    <div class="cell large-auto">
                        <div class="breadcrumbs-container">
                            <ol class="breadcrumbs-listing" itemscope=""
                                itemtype="http://schema.org/BreadcrumbList">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                        <span itemprop="name">Home</span>
                                    </a>
                                    <meta itemprop="position" content="1">
                                </li>
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                       title="Breadcrumb link 1">
                                        <span itemprop="name">Breadcrumb link 1</span>
                                    </a>
                                    <meta itemprop="position" content="2">
                                </li>

                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                       title="Breadcrumb link 2">
                                        <span itemprop="name">Breadcrumb link 2</span>
                                    </a>
                                    <meta itemprop="position" content="3">
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="cell large-shrink">
                        <div class="post_header_share">
                            <span>share</span>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-linkedin"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-facebook"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-twitter"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-whatsapp"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-envelope"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-link fa-rotate-90"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Search Results Detail</h4>
                </div>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x">

                    <div class="cell large-8 small-12">
                        <a href="#sideBarBlock" class="button eb_btn shrunk expanded hide-for-large" data-smooth-scroll="sideBarBlock">Book now</a>
                        <div class="div eb_cms_content formatted">
                            <h4>Overview</h4>
                            <p>This two day SSSTS training course is intended for those who have, or are about to acquire,
                                supervisory responsibilities. Through completing the course supervisors will be provided with an
                                ability to understand health, safety, welfare and environmental issues, as well as their legal
                                responsibilities relevant to their work activities. It will highlight the requirement to promote
                                health and safety to supervise effectively.</p>

                            <p>The SSSTS course is endorsed by the United Kingdom Contractors Group as the standard training for
                                all supervisors working on UKCG sites.</p>

                            <p>Delegates under the age of 18 must provide written consent from a parent or guardian to
                                successfully attend.</p>

                            <p><strong>* PLEASE NOTE - It will take approximately 6-8 weeks for the CITB to issue certification
                                    *</strong></p>

                            <h4>Objectives</h4>
                            <p>On successful completion of the SSSTS training course delegates that are given supervisory
                                responsibilities will understand:</p>

                            <ul>
                                <li>Why they are carrying out their identified duties</li>
                                <li>What is expected of them</li>
                                <li>Ensure they contribute to the safety of the workpltace</li>
                            </ul>


                            <h4>Agenda</h4>
                            <p>The SSSTS training course willt cover the following:</p>

                            <ul>
                                <li>Health & Safety at Work Act</li>
                                <li>The Role of the Supervisor</li>
                                <li>Behavioural Safety in the Workplace</li>
                                <li>PPE</li>
                                <li>Site Inductions & Toolbox Talks</li>
                                <li>Risk assessments and the need for method statements</li>
                                <li>Monitor site activities effectively</li>
                            </ul>

                            <h4>Duration</h4>
                            <p>2 Days</p>

                            <h4>Assessment</h4>
                            <p>At the end of the CITB SSSTS course delegates are required to sit an exam paper consisting of 25
                                questions, of which 4 are safety critical and require a short written answer. Multiple choice
                                questions are worth 1 point each and safety critical questions are worth 1-3 points each. In
                                order to pass the course delegates MUST answer ALL of safety critical questions correctly. The
                                SSSTS Course pass rate is 80% (24 out of 30)</p>

                            <p>Delegates who successfully complete the projects and assessment are awarded the CITB SSSTS - Site
                                Supervision Safety Training Scheme certificate which is valid for 5 years.</p>

                            <h4>Who Should Attend?</h4>
                            <p>This course is aimed at first line managers, foreman, gangers, team leaders, supervisors and
                                those who have, or are about to acquire supervisory responsibilities.</p>

                            <h4>Suggested Progression</h4>
                            <p>The Suggested progression would be the five day CITB Site Management Safety Training Scheme
                                (SMSTS) training course.</p>
                        </div>
                    </div>

                    <div class="cell large-4 small-12" id="sideBarBlock">
                        <div class="sidebar_block">
                            <div class="venue_location">
                                <h5>Venue</h5>
                                <p>18 Ivy Street, Rainham (Kent)
                                    ME8 8BE</p>

                                <div class="responsive-embed widescreen">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2491.1510879068446!2d0.609757316097655!3d51.36351907961176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8d3208b465351%3A0xb26976e4eb59add!2s18+Ivy+St%2C+Rainham%2C+Gillingham+ME8+8BE%2C+UK!5e0!3m2!1sen!2sro!4v1545122855662"
                                            frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar_block">
                            <div class="book_form" id="bookNow">
                                <div class="book_form_head text-center">Book Now</div>
                                <div class="book_form_body">
                                    <div class="order-summary-details">
                                        <div class="order-summary-head">
                                            <div class="name">Course
                                                Name 0
                                            </div>
                                        </div>
                                        <div class="order-summary-detail"><span class="fa fa-calendar"></span>01
                                            Jan
                                            19 - 03 Jan 19
                                        </div>
                                        <div class="order-summary-detail"><span class="fa fa-clock-o"></span>08:00
                                            -
                                            17:00
                                        </div>
                                        <div class="order-summary-detail"><span class="fa fa-map-marker"></span>Dartford.
                                            DA1 1QZ
                                        </div>

                                        <div class="order-summary-detail"><strong>Delivery:</strong> Day Release</div>

                                        <div class="order-summary-detail information"><span class="fa fa-info-circle"></span>Please
                                            note this
                                            course will run on the following dates: 7th, 14th, 21st, 28th January & 4th February
                                            2019
                                        </div>

                                        <div class="order-summary-detail information"><span class="icon-g">G</span>Please note
                                            this
                                            course will run on the following dates: 7th, 14th, 21st, 28th January & 4th February
                                            2019
                                        </div>
                                    </div>

                                    <form action="/" method="" enctype="multipart/form-data">
                                        <div class="inline_input">
                                            <label for="numberOfDelegates">No. of people</label>
                                            <div class="custom_select shade">
                                                <select name="" id="numberOfDelegates" onchange="updateTotal()">
                                                    <option value="1">1</option>
                                                    <option value="2" selected>2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                </select>
                                            </div>

                                        </div>

                                        <div class="info resize">Price per person <span
                                                    class="site_currency">&pound;</span><span id="bookinValue">250</span> <sup>ex
                                                VAT</sup></div>


                                        <div class="info total_price">Total Price <span
                                                    class="site_currency">&pound;</span><span id="bookinValue">500</span> <sup>ex
                                                VAT</sup></div>


                                        <div class="book_form_footer">
                                            <button type="submit" class="button eb_btn shrunk expanded">Book now</button>
                                        </div>
                                    </form>

                                </div>
                            </div>

                        </div>
                        <div class="sidebar_block">
                            <div class="course_extra">
                                <div class="course_extra_text">In house training</div>
                                <div class="course_extra_cta">
                                    Booking for more than 8? <a href="#">click here</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cell small-12 large-10">
                <a href="#bookNow" class="button eb_btn shrunk hub hide-for-small-only" data-smooth-scroll>Book now</a>
            </div>

        </div>
    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container up-down just-down">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center just-down">
                    <h5 class="eb_headline eb_headline_post">FAQs</h5>
                </div>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <!--
                    https://foundation.zurb.com/sites/docs/accordion.html
                    https://foundation.zurb.com/sites/docs/accordion.html#js-options
                -->
                <ul class="accordion" data-accordion>
                    <li class="accordion-item is-active" data-accordion-item>
                        <a href="#" class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a>
                        <div class="accordion-content" data-tab-content>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.</p>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.</p>

                            <div class="text-right">
                                <a href="" class="button eb_btn">Read more</a>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <a href="#" class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a>
                        <div class="accordion-content" data-tab-content>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.</p>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.</p>

                            <div class="text-right">
                                <a href="" class="button eb_btn">Read more</a>
                            </div>
                        </div>
                    </li>
                    <li class="accordion-item" data-accordion-item>
                        <a href="#" class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a>
                        <div class="accordion-content" data-tab-content>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.</p>

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.</p>

                            <div class="text-right">
                                <a href="" class="button eb_btn">Read more</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="cell small-12 large-10 text-center">
                <a href="#bookNow" class="button eb_btn shrunk hub hide-for-small-only " data-smooth-scroll>Book now</a>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area up-down back_white">
    <div class="feedback-wrap">
        <div class="feedback">
            <div class="feedback-slider">
                <?php for ($i = 0; $i < 10; $i++): ?>
                    <div class="feedback-slider-item">
                        <div class="feedback-element">
                            <div class="title">So simple, will 100% book again through EasyBook training</div>
                            <div class="quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                                ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                Nulla consequat
                                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                            </div>
                            <div class="author">Darren Rogan</div>
                            <div class="author-comp">Rogan Industries</div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area up-down small just-up">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell small-12 large-10">
                <h5 class="text-center">Other course dates</h5>
            </div>
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x align-center align-middle">
                    <div class="cell large-shrink">
                        <div class="table_info"><span class="fa fa-envelope"></span>Save & Send <i>?</i></div>
                        <div class="table_info"><span class="fa fa-g">G</span> Identifies courses which are eligible for the CITB Grant</div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 large-12 xlarge-10">
                <?php for ($x = 0; $x < 1; $x++): ?>
                    <div class="eb_search_table">
                        <div class="eb_search_table_header">
                            <div class="eb_search_table_row">
                                <div class="eb_search_table_data"><span class="name">18 Stree Name, County (City) <span>xx.x miles away</span></span></div>
                                <div class="eb_search_table_data large-text-right"><a href="#" class="table_head_link">View on map</a></div>
                            </div>

                        </div>
                        <div class="eb_search_table_body">
                            <div class="eb_search_table_row defined">
                                <div class="eb_search_table_data"><span class="table_sub_title">Course</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Start Date</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">End Date</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Delivery</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Price <sup>exVAT</sup></span></div>
                                <div class="eb_search_table_data"></div>
                                <div class="eb_search_table_data"></div>
                            </div>
                            <?php for ($i = 0; $i < 7; $i++) : ?>
                                <div class="eb_search_table_row defined <?php if ($i == 3): ?>item_on_sale<?php endif; ?>">
                                    <div class="eb_search_table_data">
                                        <a href="#" title="CITB SSSTS <?php echo $i ?> Day Course" class="name">CITB SSSTS <?php echo $i ?> Day Course <?php if ($i == 3): ?>
                                                <sup>sale</sup><?php endif; ?></a></div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">0<?php echo $i + 1 ?> Jan 10</span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">31 Dec 10</span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">Block</span></div>
                                    <div class="eb_search_table_data">
                                        <span class="item_price <?php if ($i == 3): ?>reduced<?php endif; ?>"><span
                                                    class="site_currency">&pound;</span>250 <?php if ($i == 3): ?><sup>&pound;350</sup><?php endif; ?> </span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <div class="item_cta equalized">
                                            <?php if ($i == 2) : ?><a href="" class="button eb_btn shrunk light"><span class="breakpoint">please</span>
                                                call</a><?php elseif ($i == 5) : ?><a href=""
                                                                                      class="button eb_btn shrunk dark">call
                                                <span class="breakpoint">to book</span></a><?php else: ?><a href="" class="button eb_btn shrunk">view
                                                <span class="breakpoint">details</span></a><?php endif; ?>
                                            <a href="#"><span class="fa fa-envelope"></span></a>
                                        </div>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <div class="item_cta"><?php if (!($i % 3)) : ?><span
                                                    class="fa fa-g">G</span><?php endif; ?> <?php if (!($i % 6)) : ?><a
                                                    href=""
                                                    class="button eb_btn shrunk">Book</a><?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <div class="eb_search_table_footer">
                            <div class="eb_search_table_row">
                                <div class="eb_search_table_data">
                                    <a href=""><span class="fa fa-plus"></span> view more courses</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>            </div>
        </div>
    </div>
</section>

<?php include('../template/footer.php'); ?>
