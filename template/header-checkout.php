<?php
/**
 * Created by PhpStorm.
 * User: Tiberiu
 * Date: 07/11/2018
 * Time: 11:29
 */

$site_url = '/';
$status = "";
if (isset($_GET['status'])): $status = $_GET['status']; endif; ?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Easybook - CSS</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700">
    <link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/app.css">
</head>
<body>
<header class="site_header">
    <div class="header_content" data-sticky-container>
        <div class="sticky checkout_header" data-sticky data-margin-top="0">
            <div class="grid-container">
                <div class="grid-x grid-padding-x align-middle">
                    <div class="cell shrink">
                        <a href="/" title="EasyBook" class="eb_logo">
                            <img src="<?php echo $site_url; ?>assets/img/eb_logo.png" alt="Easy Book" class="eb_logo_image_stuck">
                        </a>
                    </div>
                    <div class="cell auto text-center">
                        <a href="tel:08000931189" class="eb_phone"><span class="fa fa-phone"></span>08000 93 11 89</a>
                    </div>
                    <div class="cell shrink">
                        <div class="site_fast_info sticky_toggle">
                            <?php if ($status) :?>
                                <a href="#" class="site_login"><span aria-hidden="true" class="fa fa-lock"></span>Log Out</a>
                                <a href="#" class="site_bookings"><img src="/assets/img/icons/my_bookings.png" alt="">My Bookings</a>
                            <?php else: ?>
                                <a href="#" class="site_login" data-toggle="logInArea"><span aria-hidden="true" class="fa fa-lock"></span>Log in</a>
                                <a href="#" class="site_cart_info"><span aria-hidden="true" class="fa fa-shopping-basket"></span>Basket
                                    <span id="cartItemCount">3</span></a>
                            <?php endif; ?>

                        </div>
                        <button data-toggle="offCanvas" class="hamburger"><span></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="off-canvas-wrapper">
    <div class="off-canvas off-canvas-absolute position-right" id="offCanvas" data-reveal-on="small medium" data-trap-focus="true" data-off-canvas>
        <div class="off-canvas-menu-container">
            <ul class="vertical menu accordion-menu" data-accordion-menu>
                <li><a href="#" role="menuitem">Courses</a>
                    <ul class="menu vertical nested">
                        <li><a href="#" role="menuitem">Classroom Courses</a>
                            <ul class="menu vertical nested">
                                <li><a href="#" role="menuitem">Construction</a></li>
                                <li><a href="#" role="menuitem">Health & Safety</a></li>
                                <li><a href="#" role="menuitem">Working at Height</a></li>
                                <li><a href="#" role="menuitem">Plant</a></li>
                                <li><a href="#" role="menuitem">First Aid</a></li>
                                <li><a href="#" role="menuitem">Fire Safety</a></li>
                                <li><a href="#" role="menuitem">Food Safety</a></li>
                            </ul>
                        </li>
                        <li><a href="#" role="menuitem">NVQs</a></li>
                        <li><a href="#" role="menuitem">E-Learning</a></li>
                        <li><a href="#" role="menuitem">In-House</a></li>
                    </ul>
                </li>
                <li><a href="#" role="menuitem">Why Choose Us?</a></li>
                <li><a href="#" role="menuitem">Meet the Team</a></li>
                <li><a href="#" role="menuitem">The Hub</a></li>
                <li><a href="#" role="menuitem">Become a Partner</a></li>
                <li><a href="#" role="menuitem">Contact Us</a></li>
            </ul>
        </div>

    </div>
    <div class="off-canvas-content" data-off-canvas-content>
        <div class="eb_site_wrap">