<?php
/**
 * Created by PhpStorm.
 * User: Tiberiu
 * Date: 07/11/2018
 * Time: 11:30
 */

?>
</div> <!-- eb_site_wrap end -->

<footer class="site_footer" id="siteFooter">
    <div class="site_footer_top">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 medium-8 large-auto">
                    <form action="" class="newsletter_form">
                        <div class="newsletter_form_header">
                            <div class="title">Newsletter Sign Up</div>
                        </div>
                        <div class="newsletter_form_items">
                            <label for="f_name2" class="eb_label invert">
                                <input type="text" name="f_name" id="f_name2" aria-label="First Name"
                                       placeholder="First Name" autocomplete="given-name" required>
                                <span>First Name</span>
                            </label>

                            <label for="l_name2" class="eb_label invert">
                                <input type="text" name="l_name" id="l_name2" aria-label="Last Name"
                                       placeholder="Last Name" autocomplete="family-name" required>
                                <span>Last Name</span>
                            </label>

                            <label for="email_add2" class="eb_label invert">
                                <input type="email" name="email_add" id="email_add2" aria-label="Email Address"
                                       placeholder="Email Address" autocomplete="email" required>
                                <span>Email Address</span>
                            </label>
                        </div>
                        <div class="newsletter_form_footer">
                            <div class="grid-x align-middle">
                                <div class="cell medium-auto">
                                    <label for="gdpr_1_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_1_2">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I agree that my data will be used and stored as outlined in the <a
                                                    href="" target="_blank">Terms and Conditions</a> on the Easybook Training website
                                        </span>
                                    </label>

                                    <label for="gdpr_2_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_2_2">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I am happy to receive newsletters and promotional information from Easybook Training Ltd
                                            </span>
                                    </label>
                                </div>
                                <div class="cell medium-shrink">
                                    <button class="button eb_btn" type="submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 medium-4 large-shrink">
                    <div class="who-did-this">
                        Website by <a href="https://www.reflectdigital.co.uk/" class="reflect-digital"
                                      title="Creative Online Marketing Agency | Reflect Digital" role="link"
                                      target="_blank">Refl<span class="reflect-e">e</span>ct
                            <span>Digital</span></a>
                    </div>
                    <a href="" class="reviews_container">
                        <img src="<?php echo $site_url; ?>assets/img/logo.png" alt="Feefo reviews">
                    </a>
                </div>
            </div>
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 medium-8 large-auto">
                    <div class="site_footer_text">
                        <p>Easybook Training Ltd partners with hundreds of highly respected and industry accredited training
                            course providers. As an agent we are not directly accredited by or associated with any examining
                            body. The courses advertised on our site are delivered by training providers who are independently
                            accredited.</p>
                    </div>

                </div>
                <div class="cell small-12 medium-4 large-shrink">
                    <div class="eb_social">
                        <a href="#" target="_blank" aria-label="Follow on LinkedIn"><span aria-hidden="true" class="fa fa-linkedin"></span></a>
                        <a href="#" target="_blank" aria-label="Like on Facebook"><span aria-hidden="true" class="fa fa-facebook"></span></a>
                        <a href="#" target="_blank" aria-label="Follow on Twitter"><span aria-hidden="true" class="fa fa-twitter"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site_footer_bottom">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell large-shrink">
                    <div><?php echo '&copy; '.date('Y'); ?> Easybook Training. All rights Reserved</div>
                </div>
                <div class="cell large-auto large-text-right">
                    <ul class="site_footer_quicklinks">
                        <li><a href="#">Join Us</a></li>
                        <li><a href="#">Price Promise</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Accessibility</a></li>
                        <li><a href="#">Legal Information</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>
</div>
<script type="text/javascript" src="<?php echo $site_url; ?>assets/js/main.js"></script>
</body>
</html>