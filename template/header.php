<?php
/**
 * Created by PhpStorm.
 * User: Tiberiu
 * Date: 07/11/2018
 * Time: 11:29
 */

$site_url = '/';
$status = "";
if (isset($_GET['status'])): $status = $_GET['status']; endif; ?>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Easybook - CSS</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700">
    <link rel="stylesheet" href="<?php echo $site_url; ?>assets/css/app.css">
</head>
<body>
<header class="site_header">
    <div class="header_top" id="headerTop">
        <div class="grid-container">
            <div class="grid-x grid-padding-x medium-up-2 large-up-3 align-right">
                <div class="cell text-center">
                    <a href="tel:08000931189" class="eb_phone"><span class="fa fa-phone"></span>08000 93 11 89</a>
                </div>
                <div class="cell text-right">
                    <div class="site_fast_info">
                        <a href="tel:08000931189" class="site_phone"><span class="fa fa-phone"></span></a>
                        <?php if ($status) :?>
                            <a href="#" class="site_login"><span aria-hidden="true" class="fa fa-lock"></span>Log Out</a>
                            <a href="#" class="site_bookings"><img src="/assets/img/icons/my_bookings_invert.png" alt="">My Bookings</a>
                        <?php else: ?>

                            <a role="button" class="site_login" data-toggle="slimLogin"><span aria-hidden="true" class="fa fa-lock"></span>Log in</a>
                            <a href="#" class="site_cart_info"><span aria-hidden="true" class="fa fa-shopping-basket"></span>Basket
                                <span id="cartItemCount">3</span></a>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="slim_login" data-toggler=".active" id="slimLogin">
            <button class="close-button" data-toggle="slimLogin">&times;</button>
            <form action="" enctype="multipart/form-data">
                <div class="grid-container fluid">
                    <div class="grid-x grid-padding-x align-right">
                        <div class="cell large-6">
                            <div class="grid-x grid-padding-x align-middle">
                                <div class="cell large-shrink">
                                    <div class="login_label"><span class="fa fa-user"></span>Log in</div>
                                </div>
                                <div class="cell large-auto">
                                    <label for="loginEmailAddress" class="login_input">
                                        <input type="email" id="loginEmailAddress" autocomplete="username" aria-label="Email Address">
                                        <span>Email Address</span>
                                    </label>
                                </div>
                                <div class="cell large-auto">
                                    <label for="loginPassword" class="login_input">
                                        <input type="password" id="loginPassword" autocomplete="current-password" aria-label="Password">
                                        <span>Password <span>(case sensitive)</span></span>
                                    </label>
                                </div>
                                <div class="cell large-shrink">
                                    <button type="submit" class="button submit_login">Log in</button>
                                    <p class="help-text hide-for-large" id="passwordHelpText"><a href="#">Forgotten password?</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="header_content" data-sticky-container>
        <div class="sticky" data-sticky data-margin-top="0" data-top-anchor="headerTop:bottom" data-btm-anchor="siteFooter:bottom">
            <div class="grid-container">
                <div class="grid-x grid-padding-x align-middle">
                    <div class="cell shrink">
                        <a href="/" title="EasyBook" class="eb_logo">
                            <img src="<?php echo $site_url; ?>assets/img/eb_logo.png" alt="Easy Book" class="eb_logo_image">
                            <img src="<?php echo $site_url; ?>assets/img/eb_logo_condensed.png" alt="" class="eb_logo_image_stuck">
                        </a>
                    </div>
                    <div class="cell auto">
                        <nav class="eb_navigation">
                            <!-- Top level navigation-->
                            <ul class="menu dropdown align-right eb_navigation_items" data-dropdown-menu>
                                <li class="has-mega-menu"><a href="#" role="menuitem">Courses</a>
                                    <ul class="menu vertical">
                                        <li>
                                            <!-- Mega menu-->
                                            <div class="eb_mega_menu">
                                                <div class="grid-x large-up-3">
                                                    <div class="cell">
                                                        <div class="eb_sub_navigation">
                                                            <ul class="menu vertical dropdown" data-dropdown-menu
                                                                data-auto-height="true">
                                                                <li><a href="#" role="menuitem">Classroom Courses</a>
                                                                    <ul class="menu nested vertical">
                                                                        <li><a href="#" role="menuitem">Construction</a>
                                                                        </li>
                                                                        <li><a href="#" role="menuitem">Health & Safety</a>
                                                                        </li>
                                                                        <li><a href="#" role="menuitem">Working at
                                                                                Height</a></li>
                                                                        <li><a href="#" role="menuitem">Plant</a></li>
                                                                        <li><a href="#" role="menuitem">First Aid</a></li>
                                                                        <li><a href="#" role="menuitem">Fire Safety</a></li>
                                                                        <li><a href="#" role="menuitem">Food Safety</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li><a href="#" role="menuitem">NVQs</a></li>
                                                                <li><a href="#" role="menuitem">E-Learning</a></li>
                                                                <li><a href="#" role="menuitem">In-House</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="cell"></div>
                                                    <div class="cell">

                                                        <!-- new & sale are classes that show ribbons -->
                                                        <div class="eb_featured offer sale">
                                                            <div class="eb_featured_image"
                                                                 style="background-image: url(//picsum.photos/520)"></div>
                                                            <div class="eb_featured_content">

                                                                <div class="eb_featured_headline">Last Minute &<br> Discounted Courses</div>
                                                                <div class="eb_featured_discount">Up to 40% off</div>

                                                                <a href="#" class="button eb_btn">view courses</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="#" role="menuitem">Why Choose Us?</a>
                                    <ul class="menu vertical">
                                        <li><a href="">Test 1</a></li>
                                        <li><a href="">Test 2</a></li>
                                        <li><a href="">Test 3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#" role="menuitem">Meet the Team</a></li>
                                <li><a href="#" role="menuitem">The Hub</a></li>
                                <li><a href="#" role="menuitem">Become a Partner</a></li>
                                <li><a href="#" role="menuitem">Contact Us</a></li>
                            </ul>
                        </nav>

                        <a href="" title="Feefo reviews" class="feefo_reviews tablet-down">
                            <img src="<?php echo $site_url; ?>assets/img/logo.png" alt="Feefo reviews">
                        </a>
                    </div>
                    <div class="cell shrink">
                        <a href="" title="Feefo reviews" class="feefo_reviews">
                            <img src="<?php echo $site_url; ?>assets/img/logo.png" alt="Feefo reviews">
                        </a>
                        <div class="site_fast_info sticky_toggle">
                            <?php if ($status) :?>
                                <a href="#" class="site_login"><span aria-hidden="true" class="fa fa-lock"></span>Log Out</a>
                                <a href="#" class="site_bookings"><img src="/assets/img/icons/my_bookings.png" alt="">My Bookings</a>
                            <?php else: ?>
                                <a href="#" class="site_login"><span aria-hidden="true" class="fa fa-lock"></span>Log in</a>
                                <a href="#" class="site_cart_info"><span aria-hidden="true" class="fa fa-shopping-basket"></span>Basket
                                    <span id="cartItemCount">3</span></a>
                            <?php endif; ?>

                        </div>
                        <button data-toggle="offCanvas" class="hamburger"><span></span></button>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header_bottom">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell">
                    <div class="eb_usp_slider">
                        <div>
                            <a href="#" class="eb_usp">
                                <img src="<?php echo $site_url; ?>assets/img/eb_cards.png" alt="Book Online Now with NO booking fees">
                                <span>Book Online Now with NO booking fees</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="eb_usp">
                                <img src="<?php echo $site_url; ?>assets/img/eb_uk.png" alt="Nationwide coverage">
                                <span>Nationwide coverage</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="eb_usp">
                                <img src="<?php echo $site_url; ?>assets/img/eb_book.png" alt="Thousands of Health and Safety courses">
                                <span>Thousands of Health and Safety courses</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="off-canvas-wrapper">
    <div class="off-canvas off-canvas-absolute position-right" id="offCanvas" data-reveal-on="small medium" data-trap-focus="true" data-off-canvas>
        <div class="off-canvas-menu-container">
            <ul class="vertical menu accordion-menu" data-accordion-menu>
                <li><a href="#" role="menuitem">Courses</a>
                    <ul class="menu vertical nested">
                        <li><a href="#" role="menuitem">Classroom Courses</a>
                            <ul class="menu vertical nested">
                                <li><a href="#" role="menuitem">Construction</a></li>
                                <li><a href="#" role="menuitem">Health & Safety</a></li>
                                <li><a href="#" role="menuitem">Working at Height</a></li>
                                <li><a href="#" role="menuitem">Plant</a></li>
                                <li><a href="#" role="menuitem">First Aid</a></li>
                                <li><a href="#" role="menuitem">Fire Safety</a></li>
                                <li><a href="#" role="menuitem">Food Safety</a></li>
                            </ul>
                        </li>
                        <li><a href="#" role="menuitem">NVQs</a></li>
                        <li><a href="#" role="menuitem">E-Learning</a></li>
                        <li><a href="#" role="menuitem">In-House</a></li>
                    </ul>
                </li>
                <li><a href="#" role="menuitem">Why Choose Us?</a></li>
                <li><a href="#" role="menuitem">Meet the Team</a></li>
                <li><a href="#" role="menuitem">The Hub</a></li>
                <li><a href="#" role="menuitem">Become a Partner</a></li>
                <li><a href="#" role="menuitem">Contact Us</a></li>
            </ul>
        </div>

    </div>
    <div class="off-canvas-content" data-off-canvas-content>
        <div class="eb_site_wrap">