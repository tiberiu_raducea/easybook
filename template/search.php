<section class="eb_internal_search">
    <form action="" class="eb_internal_search_form" id="internalSearchForm" data-toggler="active">
        <div class="grid-container fluid">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <div class="mobile_behave grid-x grid-padding-x">
                        <div class="cell medium-6">
                            <label for="selectCategory" class="eb_select">
                                <select name="search_category" id="selectCategory">
                                    <option value="" selected hidden>Select a category</option>
                                    <option value="Category 1">Category 1</option>
                                    <option value="Category 2">Category 2</option>
                                    <option value="Category 3">Category 3</option>
                                    <option value="Category 4">Category 4</option>
                                    <option value="Category 5">Category 5</option>
                                </select>
                            </label>
                        </div>
                        <div class="cell medium-6">
                            <label for="selectCourseTitle" class="eb_select">
                                <select name="search_category" id="selectCourseTitle">
                                    <option value="" selected hidden>Select a course title</option>
                                    <option value="Course Title 1">Course Title 1</option>
                                    <option value="Course Title 2">Course Title 2</option>
                                    <option value="Course Title 3">Long Course Title that goes on two
                                        lines on some screens
                                    </option>
                                    <option value="Course Title 4">Even longer this Course Title 4
                                    </option>
                                    <option value="Course Title 5">Some long Course Title 5</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="cell small-12 large-6">
                    <div class="mobile_behave grid-x grid-padding-x">
                        <div class="cell medium-shrink">
                            <label for="ebPostcode" class="eb_clean_input post_code">
                                <input type="text" id="ebPostcode" placeholder="Postcode">
                            </label>
                        </div>
                        <div class="cell medium-auto">
                            <label for="ebDates" class="eb_clean_input edge">
                                <span class="fa fa-calendar" aria-hidden="true"></span>
                                <input type="text" id="ebDates" name="daterange"
                                       placeholder="Start between">
                            </label>
                        </div>
                        <div class="cell large-shrink">
                            <button type="submit" value="searchEasyBook" class="button eb_btn">Search</button>
                        </div>
                    </div>

                    <a href="#" data-toggle="internalSearchForm closeOnSmall" class="button eb_btn expanded toggle_search">Search courses</a>

                    <div class="text-center hide" data-toggler="hide-for-large hide" id="closeOnSmall">
                        <a href="#" data-toggle="internalSearchForm closeOnSmall">&times; close</a>
                    </div>
                </div>

            </div>
        </div>
    </form>
</section>
