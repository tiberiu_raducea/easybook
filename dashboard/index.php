<?php include('../template/header-checkout.php'); ?>

<section class="eb_login_area is_hidden <?php if ($status) : ?>logged_in<?php endif; ?>" data-toggler="is_hidden" id="logInArea">
    <div class="grid-container">

        <?php if ($status) : ?>
            <div class="grid-x grid-padding-x align-middle">
                <div class="cell large-auto">
                    <div class="user_greeting">
                        <h4><span class="fa fa-user"></span> Welcome back, Danielle Ross-Davies</h4>
                    </div>
                </div>
                <div class="cell large-shrink">
                    <div class="user_cta">
                        <a href="#" class="edit_details">Edit Details</a>
                        <a href="#" class="edit_details switch_account"><span class="fa fa-undo fa-flip-horizontal"></span> Switch to Reflect Digital to view all Reflect Digital Bookings</a>
                        <a href="#" class="button eb_btn shrunk">Log out</a>
                    </div>
                </div>
            </div>

        <?php else: ?>

            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-9">
                    <form action="" class="eb_login_form">
                        <div class="grid-x grid-padding-x  align-middle">
                            <div class="cell large-shrink">
                                <label for="logUserEmail" class="eb_login_form_headline">
                                    <span class="fa fa-user"></span>
                                    Log in
                                </label>
                            </div>
                            <div class="cell large-auto">
                                <label class="eb_label invert">
                                    <input type="email" id="logUserEmail" aria-label="Email Address"
                                           autocomplete="user-name"
                                           placeholder="Email Address" required="">
                                    <span>Email Address</span>
                                </label>
                            </div>
                            <div class="cell large-auto">
                                <label for="" class="eb_label invert">
                                    <input type="password" placeholder="Password (case sensitive)"
                                           aria-label="case sensitive password" autocomplete="current-password"
                                           required=""
                                           aria-describedby="passwordHelpText">
                                    <span>Password (case sensitive)</span>
                                </label>
                                <p class="help-text" id="passwordHelpText"><a href="#">I've forgotten my
                                        password</a></p>
                            </div>
                            <div class="cell large-shrink">
                                <button type="submit" class="button eb_btn" aria-label="Log in">Log in</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 large-3">
                    <a href="#" class="eb_create_account">
                        <span>Don't have an EasyBook login?</span>
                        <span>Create an account</span>
                    </a>
                </div>
            </div>

        <?php endif; ?>
    </div>

</section>

<section class="eb_content_area back_light_purple up-down small">
    <div class="grid-container show-for-small-only">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <a type="submit" data-toggle="showDashboardFilters" class="button eb_btn">Show Filters</a>
            </div>
        </div>
    </div>
    <div id="showDashboardFilters"  class="dashboard_filters_mobile" data-toggler="active">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12">
                    <div class="button_filters">
                        <a role="button" class="button_filter active"><span>All Bookings</span></a>
                        <a role="button" class="button_filter"><span>Pending Transfer</span></a>
                        <a role="button" class="button_filter"><span>Copy of current certificate not supplied</span></a>
                        <a role="button" class="button_filter"><span>Delegate did not attend</span></a>
                        <a role="button" class="button_filter"><span>Awaiting Joining Instructions</span></a>
                        <a role="button" class="button_filter"><span>Awaiting Certificate</span></a>
                    </div>
                </div>
            </div>
        </div>
        <form action="" class="dashboard_search">
            <div class="grid-container fluid">
                <div class="grid-x grid-padding-x small-collapse">
                    <div class="cell large-5">
                        <label for=""></label>
                        <div class="grid-x grid-padding-x">
                            <div class="cell large-auto">
                                <div class="dashboard_search_label">Booked Between</div>
                                <label for="ebDatesBookedBetween" class="eb_clean_input">
                                    <span class="fa fa-calendar" aria-hidden="true"></span>
                                    <input type="text" id="ebDatesBookedBetween" name="ebDatesBookedBetween" placeholder="Start between">
                                </label>
                            </div>
                            <div class="cell large-auto">
                                <div class="dashboard_search_label">Starting Between</div>
                                <label for="ebDatesStartingBetween" class="eb_clean_input">
                                    <span class="fa fa-calendar" aria-hidden="true"></span>
                                    <input type="text" id="ebDatesStartingBetween" name="ebDatesStartingBetween" placeholder="Start between">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="cell large-7">
                        <div class="dashboard_search_label">or filter by course, delegate or invoice number</div>
                        <div class="grid-x grid-padding-x">
                            <div class="cell large-auto">
                                <label for="dashboardSelectCourse" class="eb_select">
                                    <select name="dashboardSelectCourse" id="dashboardSelectCourse">
                                        <option value="" selected hidden>Select course</option>
                                        <option value="Course Title 1">Course Title 1</option>
                                        <option value="Course Title 2">Course Title 2</option>
                                        <option value="Course Title 3">Long Course Title that goes on two
                                            lines on some screens
                                        </option>
                                        <option value="Course Title 4">Even longer this Course Title 4
                                        </option>
                                        <option value="Course Title 5">Some long Course Title 5</option>
                                    </select>
                                </label>
                            </div>
                            <div class="cell large-auto">
                                <label for="dashboardSelectDelegate" class="eb_select">
                                    <select name="dashboardSelectDelegate" id="dashboardSelectDelegate">
                                        <option value="" selected hidden>Select Delegate</option>
                                        <option value="Course Title 1">Danielle Ross-Davies</option>
                                        <option value="Course Title 2">David Jones</option>
                                    </select>
                                </label>
                            </div>
                            <div class="cell large-auto">
                                <label for="dashboardInvoiceNumber" class="eb_clean_input">
                                    <input type="text" name="dashboardInvoiceNumber" id="dashboardInvoiceNumber" aria-label="Invoice Number" placeholder="Invoice Number">
                                </label>
                            </div>
                            <div class="cell large-shrink">
                                <button type="submit" value="searchEasyBook" class="button eb_btn">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>


<section class="eb_content_area up-down small">
    <div class="grid-container fluid">
        <div class="grid-x grid-padding-y grid-padding-x">

            <div class="cell large-auto order-1 large-order-2">
                <div class="dashboard_actions">
                   <div class="grid-x grid-padding-x">
                        <div class="cell medium-auto">
                            <form>
                                <div class="grid-x grid-padding-x align-middle">
                                    <div class="cell medium-auto">
                                        <div class="text-left medium-text-right eb_purple">Results per page</div>
                                    </div>
                                    <div class="cell medium-auto large-shrink">
                                        <div class="custom_select shade white">
                                            <label for="resultsPerPage">
                                                <select name="" id="resultsPerPage">
                                                    <option value="10" selected>10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="all">All</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="cell medium-auto large-shrink">
                            <button class="button eb_btn shrunk">Export to csv</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cell large-auto order-2 large-order-1">
                <nav aria-label="Pagination">
                    <ul class="pagination text-center large-text-left">
                        <li class="pagination-previous"><a href="#" aria-label="Previous page">Previous <span class="show-for-sr">page</span></a></li>
                        <li><a href="#" aria-label="Page 1">1</a></li>
                        <li class="current"><span class="show-for-sr">You're on page</span> 2</li>
                        <li><a href="#" aria-label="Page 3">3</a></li>
                        <li><a href="#" aria-label="Page 4">4</a></li>
                        <li><a href="#" aria-label="Page 5">5</a></li>
                        <li><a href="#" aria-label="Page 6">6</a></li>
                        <li class="pagination-next"><a href="#" aria-label="Next page">Next <span class="show-for-sr">page</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <div class="table-scroll">
                    <table class="dashboard_table" id="dashboardTable">
                        <thead>
                            <tr>
                                <th onclick="sortTable(0)"><span class="dashboard_table_sort">Course</span></th>
                                <th onclick="sortTable(1)"><span class="dashboard_table_sort">Start Date</span></th>
                                <th onclick="sortTable(2)"><span class="dashboard_table_sort">Info Sent</span></th>
                                <th onclick="sortTable(3)"><span class="dashboard_table_sort">Delegate</span></th>
                                <th onclick="sortTable(4)"><span class="dashboard_table_sort">Booked by</span></th>
                                <th onclick="sortTable(5)"><span class="dashboard_table_sort">Invoice</span></th>
                                <th onclick="sortTable(6)"><span class="dashboard_table_sort">Cart Sent</span></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($i = 0; $i < 10; $i++): ?>
                            <tr data-fetch-row="<?php echo $i ?>" id="fetchRow<?php echo $i ?>" data-open="rowModal<?php echo $i ?>">
                                <td><span class="value strong">CITB SSSTS <?php echo rand(1,9) ?> Day Course <?php if ($i == 2) : ?><span class="notice"><i class="fa fa-exclamation-circle"></i> Did not attend</span><?php endif; ?></span></td>
                                <td><span class="value">0<?php echo rand(1,9) ?> Jan' 17</span></td>
                                <td><span class="value">31 Dec' 10</span></td>
                                <td><span class="value">Mr David Jones</span></td>
                                <td><span class="value">Mrs Danielle Ross- Davies</span></td>
                                <td><span class="value">1835<?php echo $i ?> <a href="#" download="" class="download"><i class="fa fa-download"></i></a></span></td>
                                <td><span class="value"><?php if ($i != 2) : ?>0<?php echo rand(1,9) ?> Feb’ 11</span><?php endif; ?></td>
                                <td><a role="button" class="button eb_btn small shrunk" data-open="rowModal<?php echo $i ?>">view more</a>

                                    <?php /* https://foundation.zurb.com/sites/docs/reveal.html#ajax */ ?>
                                    <div class="reveal small" id="rowModal<?php echo $i ?>" data-reveal style="display: none">
                                        <div class="modal_content">
                                            <div class="modal_detail">
                                                <div class="header">
                                                    <div>
                                                        <span class="course">CITB SMSTS 5 Day Course</span>
                                                        <span class="info">Havant</span>
                                                    </div>
                                                    <div>
                                                        <a href="#"><span class="fa fa-envelope"></span>Email Easybook Training</a>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <ul>
                                                        <li>
                                                            <span class="title">Start Date:</span>
                                                            <div class="text">01 January 2011</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Info Sent:</span>
                                                            <div class="text">31 December 2010</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Delegate:</span>
                                                            <div class="text">Mr David Jones</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Booked by:</span>
                                                            <div class="text">Mrs Danielle Ross-Davies</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Invoice:</span>
                                                            <div class="text">18352 <a href=""><span class="fa fa-download"></span>Download</a></div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Cert Sent:</span>
                                                            <div class="text">01 January 2011</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Pre Requisites Supplied:</span>
                                                            <div class="text">No</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Date Booked:</span>
                                                            <div class="text">19-09-2018</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Cost:</span>
                                                            <div class="text">£618.00</div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Delegates  Email:</span>
                                                            <div class="text"><a href="mailto:hayley.hoppins@balfourbeatty.com">hayley.hoppins@balfourbeatty.com</a></div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Contact Email:</span>
                                                            <div class="text"><a href="mailto:hayley.hoppins@balfourbeatty.com">hayley.hoppins@balfourbeatty.com</a></div>
                                                        </li>
                                                        <li>
                                                            <span class="title">Contact Tel:</span>
                                                            <div class="text">0191 228 2266</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="modal_image" style="background-image: url(//picsum.photos/330/720/?image=<?php echo $i + 33 ?>)"></div>
                                        </div>
                                        <button class="close-button" data-close aria-label="Close modal" type="button">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            <?php endfor; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="cell">

            </div>
        </div>
    </div>
</section>
<?php include('../template/footer.php'); ?>
