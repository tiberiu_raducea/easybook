<?php include('../template/header-checkout.php'); ?>
    <section class="eb_login_area text-center large-text-left is_hidden <?php if ($status) : ?>logged_in<?php endif; ?>" data-toggler="is_hidden"
             id="logInArea">
        <div id="logInAreaContent" class="eb_login_area_content hide_login" data-toggler="hide_login">
            <?php if ($status) : ?>
                <div class="grid-container">
                    <div class="grid-x grid-padding-x align-center">
                        <div class="cell small-12 large-10">
                            <div class="grid-x grid-padding-x align-middle">
                                <div class="cell large-auto">
                                    <div class="user_greeting">
                                        <h3><span class="fa fa-user"></span> Welcome back, David Jones</h3>
                                    </div>
                                </div>
                                <div class="cell large-shrink">
                                    <div class="user_cta">
                                        <a href="" class="edit_details">Edit Details</a>
                                        <a href="" class="button eb_btn shrunk">Log out</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>


                <div class="grid-container">
                    <div class="grid-x grid-padding-x align-center">
                        <div class="cell small-12 large-10">
                            <div class="grid-x grid-padding-x">
                                <div class="cell small-12 large-9">
                                    <form action="" class="eb_login_form">
                                        <div class="grid-x grid-padding-x  align-middle">
                                            <div class="cell large-shrink">
                                                <label for="logUserEmail" class="eb_login_form_headline">
                                                    <span class="fa fa-user"></span>
                                                    Log in
                                                </label>
                                            </div>
                                            <div class="cell large-auto">
                                                <label class="eb_label invert">
                                                    <input type="email" id="logUserEmail" aria-label="Email Address"
                                                           autocomplete="user-name"
                                                           placeholder="Email Address" required="">
                                                    <span>Email Address</span>
                                                </label>
                                            </div>
                                            <div class="cell large-auto">
                                                <label for="" class="eb_label invert">
                                                    <input type="password" placeholder="Password (case sensitive)"
                                                           aria-label="case sensitive password"
                                                           autocomplete="current-password"
                                                           required=""
                                                           aria-describedby="passwordHelpText">
                                                    <span>Password (case sensitive)</span>
                                                </label>
                                                <p class="help-text" id="passwordHelpText"><a href="#">I've forgotten my
                                                        password</a></p>
                                            </div>
                                            <div class="cell large-shrink">
                                                <button type="submit" class="button eb_btn" aria-label="Log in">Log in
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="cell small-12 large-3">
                                    <a href="#" class="eb_create_account">
                                        <span>Don't have an EasyBook login?</span>
                                        <span>Create an account</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <button class="close-button " data-toggle="logInAreaContent">&times;</button>
        </div>
        <button class="button eb_btn shrunk toggle_login" data-toggle="logInAreaContent">LOGIN</button>
    </section>

    <section class="eb_content_area">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope=""
                            itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="eb_checkout_area">
        <form action="" enctype="multipart/form-data" class="eb_checkout_form">
            <div class="grid-container">
                <div class="grid-x grid-padding-x align-center">
                    <div class="cell small-12 large-10">
                        <div class="checkout-stages up-down just-down">
                            <div class="checkout-stage-container">
                                <div class="checkout-stage-tab-container" id="sticker">
                                    <div class="grid-x grid-padding-x">
                                        <div class="cell small-12 large-7">

                                            <div class="eb_headline_block fluid text-center just-down">
                                                <h4 class="eb_headline">Checkout</h4>
                                            </div>

                                            <div class="checkout-stage-head">
                                                <span class="line"></span>

                                                <div class="checkout-stage current-stage" data-stage-tab="yourDetails">
                                                    <div class="checkout-stage-content">
                                                    <span class="checkout-stage-title" data-stage-count="1"><span
                                                                class="text">Your details</span></span>
                                                    </div>
                                                </div>
                                                <div class="checkout-stage" data-stage-tab="delegateDetails">
                                                    <div class="checkout-stage-content">
                                                    <span class="checkout-stage-title" data-stage-count="2"><span
                                                                class="text">Delegate(s) details</span></span>
                                                    </div>
                                                </div>
                                                <div class="checkout-stage" data-stage-tab="paymentTab">
                                                    <div class="checkout-stage-content">
                                                    <span class="checkout-stage-title" data-stage-count="3"><span
                                                                class="text">Payment</span></span>
                                                    </div>
                                                </div>
                                                <div class="checkout-stage" data-stage-tab="orderConfirmation">
                                                    <div class="checkout-stage-content">
                                                    <span class="checkout-stage-title" data-stage-count="4"><span
                                                                class="text">Order confirmation</span></span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="checkout-stage-tab current-stage" id="yourDetails">
                                                <div class="checkout-stage-tab-content">
                                                    <div class="email-address-area">
                                                        <div class="stage-headline">Your details</div>
                                                        <div class="eb_checkout_form_items">
                                                            <label for="checkout_title"
                                                                   class="eb_label expanded eb_select">
                                                                <select name="checkout_title" id="checkout_title"
                                                                        required>
                                                                    <option value="" selected="selected" hidden>Title
                                                                    </option>
                                                                    <option value="mr">Mr</option>
                                                                    <option value="mrs">Mrs</option>
                                                                    <option value="miss">Miss</option>
                                                                    <option value="ms">Ms</option>
                                                                    <option value="mx">Mx</option>
                                                                    <option value="sir">Sir</option>
                                                                    <option value="dr">Dr</option>
                                                                    <option value="lady">Lady</option>
                                                                    <option value="lord">Lord</option>
                                                                </select>
                                                            </label>

                                                            <label for="checkout_f_name" class="eb_label">
                                                                <input type="text" name="checkout_f_name"
                                                                       id="checkout_f_name"
                                                                       aria-label="First Name"
                                                                       placeholder="First Name*"
                                                                       autocomplete="given-name"
                                                                       required>
                                                                <span>First Name*</span>
                                                            </label>

                                                            <label for="checkout_l_name" class="eb_label">
                                                                <input type="text" name="checkout_l_name"
                                                                       id="checkout_l_name"
                                                                       aria-label="Last Name"
                                                                       placeholder="Last Name*"
                                                                       autocomplete="family-name"
                                                                       required>
                                                                <span>Last Name*</span>
                                                            </label>

                                                            <label for="checkout_company_name"
                                                                   class="eb_label expanded">
                                                                <input type="text" name="checkout_company_name"
                                                                       id="checkout_company_name"
                                                                       aria-label="Company Name"
                                                                       placeholder="Company Name*"
                                                                       autocomplete="organization">
                                                                <span>Company Name*</span>
                                                            </label>

                                                            <label for="checkout_job_title" class="eb_label expanded">
                                                                <input type="text" name="checkout_job_title"
                                                                       id="checkout_job_title" aria-label="Job Title"
                                                                       placeholder="Job Title"
                                                                       autocomplete="organization-title">
                                                                <span>Job Title*</span>
                                                            </label>

                                                            <label for="checkout_email_add" class="eb_label expanded">
                                                                <input type="email" name="checkout_email_add"
                                                                       id="checkout_email_add"
                                                                       aria-label="Email Address"
                                                                       placeholder="Email Address*" autocomplete="email"
                                                                       required>
                                                                <span>Email Address*</span>
                                                            </label>

                                                            <label for="checkout_phone_no" class="eb_label expanded">
                                                                <input type="tel" name="checkout_phone_no"
                                                                       id="checkout_phone_no" aria-label="Phone Number"
                                                                       placeholder="Phone Number*" autocomplete="tel"
                                                                       required>
                                                                <span>Phone Number*</span>
                                                            </label>

                                                            <label for="checkout_postcode" class="eb_label expanded">
                                                                <input type="text" name="checkout_postcode"
                                                                       id="checkout_postcode"
                                                                       aria-label="Postcode lookup"
                                                                       placeholder="Postcode*"
                                                                       autocomplete="postal-code" required>
                                                                <span>Postcode*</span>
                                                                <a href="#" id="postCodeLookup" class="address_lookup">lookup
                                                                    address</a>
                                                            </label>
                                                        </div>


                                                        <a href="" id="manualAddress" class="manual-address">Enter
                                                            address
                                                            manually</a>

                                                        <div class="manual-address-fields" id="manualAddressFields"
                                                             style="display: none;">
                                                            <div class="eb_checkout_form_items">
                                                                <label for="manualCustomerHouseNumber"
                                                                       class="eb_label expanded">
                                                                    <input type="text" id="manualCustomerHouseNumber"
                                                                           class="stage-input"
                                                                           autocomplete="street-address"
                                                                           placeholder="House Number">
                                                                    <span>House Number</span>

                                                                </label>
                                                                <label for="manualCustomerStreet"
                                                                       class="eb_label expanded">
                                                                    <input type="text" id="manualCustomerStreet"
                                                                           class="stage-input"
                                                                           autocomplete="address-line1"
                                                                           placeholder="Stree Name">
                                                                    <span>Street Name</span>
                                                                </label>
                                                                <label for="manualCustomerCity"
                                                                       class="eb_label expanded">
                                                                    <input type="text" id="manualCustomerCity"
                                                                           class="stage-input" autocomplete="city"
                                                                           placeholder="City">
                                                                    <span>City</span>

                                                                </label>
                                                                <label for="manualCustomerCounty"
                                                                       class="eb_label expanded">
                                                                    <input type="text" id="manualCustomerCounty"
                                                                           class="stage-input" autocomplete="county"
                                                                           placeholder="County">
                                                                    <span>County</span>

                                                                </label>
                                                                <label for="manualCustomerPostCode"
                                                                       class="eb_label expanded">
                                                                    <input type="text" id="manualCustomerPostCode"
                                                                           class="stage-input"
                                                                           autocomplete="postal-code"
                                                                           placeholder="Post code">
                                                                    <span>Post code</span>
                                                                </label>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="checkout-stage-tab-footer">
                                                    <a href="#" class="button eb_btn"
                                                       data-stage-continue="delegateDetails">Next</a>
                                                </div>
                                            </div>

                                            <div class="checkout-stage-tab" id="delegateDetails">
                                                <div class="checkout-stage-tab-content">
                                                    <?php for ($i = 0; $i < 3; $i++) : ?>
                                                        <div class="stage_details <?php if ($i == 0) : ?>complete<?php elseif ($i == 1) : ?>open<?php else: ?>closed<?php endif; ?>"
                                                             id="delegateDetails<?php echo $i ?>"
                                                             data-toggler="open closed">
                                                            <div class="stage_details_head">
                                                                <div class="details">Delegate Details</div>
                                                                <div class="course_name">Course Name Two <span
                                                                            class="stage_details_display"
                                                                            id="displayDelegateDetails<?php echo $i ?>">Mr John Smith</span>
                                                                </div>
                                                                <div class="stage_details_control">
                                                                    <a role="button" class="edit_button"
                                                                       id="editButton<?php echo $i ?>">edit</a>
                                                                    <a role="button"
                                                                       class="control_button <?php if ($i == 1) : ?>negative<?php endif; ?>"
                                                                       id="controlButton<?php echo $i ?>"
                                                                       data-delegate="delegateDetails<?php echo $i ?>"
                                                                       data-toggle="delegateDetails<?php echo $i ?> controlButton<?php echo $i ?>"
                                                                       data-toggler=".negative"></a>
                                                                </div>
                                                            </div>
                                                            <div class="stage_details_body">
                                                                <label for="useStoredData<?php echo $i ?>"
                                                                       class="eb_checkbox large">
                                                                    <input type="checkbox"
                                                                           id="useStoredData<?php echo $i ?>">
                                                                    <span>Use Contact Details already supplied</span>
                                                                </label>
                                                                <div class="eb_checkout_form_items">
                                                                    <label for="checkout_title<?php echo $i ?>"
                                                                           class="eb_label expanded eb_select">
                                                                        <select name="checkout_title"
                                                                                id="checkout_title<?php echo $i ?>"
                                                                                required>
                                                                            <option value="" selected="selected"
                                                                                    hidden>Title
                                                                            </option>
                                                                            <option value="mr">Mr</option>
                                                                            <option value="mrs">Mrs</option>
                                                                            <option value="miss">Miss</option>
                                                                            <option value="ms">Ms</option>
                                                                            <option value="mx">Mx</option>
                                                                            <option value="sir">Sir</option>
                                                                            <option value="dr">Dr</option>
                                                                            <option value="lady">Lady</option>
                                                                            <option value="lord">Lord</option>
                                                                        </select>
                                                                    </label>

                                                                    <label for="stage_f_name<?php echo $i ?>"
                                                                           class="eb_label">
                                                                        <input type="text"
                                                                               name="stage_f_name<?php echo $i ?>"
                                                                               id="stage_f_name<?php echo $i ?>"
                                                                               aria-label="First Name"
                                                                               placeholder="First Name*"
                                                                               autocomplete="given-name"
                                                                               required>
                                                                        <span>First Name*</span>
                                                                    </label>

                                                                    <label for="stage_l_name<?php echo $i ?>"
                                                                           class="eb_label">
                                                                        <input type="text"
                                                                               name="stage_l_name<?php echo $i ?>"
                                                                               id="stage_l_name<?php echo $i ?>"
                                                                               aria-label="Last Name"
                                                                               placeholder="Last Name*"
                                                                               autocomplete="family-name"
                                                                               required>
                                                                        <span>Last Name*</span>
                                                                    </label>

                                                                    <label for="stage_email_add<?php echo $i ?>"
                                                                           class="eb_label expanded">
                                                                        <input type="email"
                                                                               name="stage_email_add<?php echo $i ?>"
                                                                               id="stage_email_add<?php echo $i ?>"
                                                                               aria-label="Email Address"
                                                                               placeholder="Email Address*"
                                                                               autocomplete="email"
                                                                               required>
                                                                        <span>Email Address*</span>
                                                                    </label>

                                                                    <label for="stage_phone_no<?php echo $i ?>"
                                                                           class="eb_label expanded">
                                                                        <input type="tel"
                                                                               name="stage_phone_no<?php echo $i ?>"
                                                                               id="stage_phone_no<?php echo $i ?>"
                                                                               aria-label="Mobile Number"
                                                                               placeholder="Mobile Number*"
                                                                               aria-describedby="phoneNoHelp<?php echo $i ?>"
                                                                               autocomplete="tel" required>
                                                                        <span>Mobile Number*</span>
                                                                    </label>
                                                                    <p class="help-text"
                                                                       id="phoneNoHelp<?php echo $i ?>">(Enter to
                                                                        receive an SMS course reminder)</p>

                                                                    <label for="stage_message<?php echo $i ?>"
                                                                           class="eb_label expanded eb_textarea">
                                                                        <textarea name="message"
                                                                                  id="stage_message<?php echo $i ?>"
                                                                                  cols="30" rows="5"
                                                                                  aria-describedby="textAreaHelp<?php echo $i ?>"></textarea>
                                                                        <span>Additional Info</span>
                                                                    </label>
                                                                    <p class="help-text"
                                                                       id="textAreaHelp<?php echo $i ?>">i.e. Learning
                                                                        Difficulties, dietary requirements- (only
                                                                        applicable on certain courses)</p>

                                                                </div>
                                                            </div>
                                                            <div class="stage_details_footer">
                                                                <button type="button"
                                                                        class="button eb_btn stage_details_submit"
                                                                        id="delegateDetailsSubmit<?php echo $i ?>">
                                                                    submit
                                                                    details
                                                                </button>
                                                            </div>
                                                        </div>
                                                    <?php endfor; ?>
                                                </div>
                                                <div class="checkout-stage-tab-footer">
                                                    <a href="" class="button eb_btn"
                                                       data-stage-continue="paymentTab">Next</a>
                                                </div>
                                            </div>

                                            <div class="checkout-stage-tab" id="paymentTab">
                                                <div class="checkout-stage-tab-content">
                                                    <div class="stage_details open">
                                                        <div class="stage_details_head">
                                                            <div class="details">Billing Details</div>
                                                        </div>
                                                        <div class="stage_details_body">
                                                            <label for="useStoredDataBilling"
                                                                   class="eb_checkbox small">
                                                                <input type="checkbox"
                                                                       id="useStoredDataBilling">
                                                                <span>Use details from <a href="#yourDetails"
                                                                                          data-stage-continue="yourDetails">Step one</a></span>
                                                            </label>
                                                            <div class="eb_checkout_form_items">
                                                                <label for="billing_title"
                                                                       class="eb_label expanded eb_select">
                                                                    <select name="billing_title"
                                                                            id="billing_title"
                                                                            required>
                                                                        <option value="" selected="selected"
                                                                                hidden>Title
                                                                        </option>
                                                                        <option value="mr">Mr</option>
                                                                        <option value="mrs">Mrs</option>
                                                                        <option value="miss">Miss</option>
                                                                        <option value="ms">Ms</option>
                                                                        <option value="mx">Mx</option>
                                                                        <option value="sir">Sir</option>
                                                                        <option value="dr">Dr</option>
                                                                        <option value="lady">Lady</option>
                                                                        <option value="lord">Lord</option>
                                                                    </select>
                                                                </label>

                                                                <label for="billing_f_name"
                                                                       class="eb_label">
                                                                    <input type="text"
                                                                           name="billing_f_name"
                                                                           id="billing_f_name"
                                                                           aria-label="First Name"
                                                                           placeholder="First Name*"
                                                                           autocomplete="given-name"
                                                                           required>
                                                                    <span>First Name*</span>
                                                                </label>

                                                                <label for="billing_l_name"
                                                                       class="eb_label">
                                                                    <input type="text"
                                                                           name="billing_l_name"
                                                                           id="billing_l_name"
                                                                           aria-label="Last Name"
                                                                           placeholder="Last Name*"
                                                                           autocomplete="family-name"
                                                                           required>
                                                                    <span>Last Name*</span>
                                                                </label>

                                                                <label for="billing_email_add"
                                                                       class="eb_label expanded">
                                                                    <input type="email"
                                                                           name="billing_email_add"
                                                                           id="billing_email_add"
                                                                           aria-label="Email Address"
                                                                           placeholder="Email Address*"
                                                                           autocomplete="email"
                                                                           required>
                                                                    <span>Email Address*</span>
                                                                </label>

                                                                <label for="billing_postcode"
                                                                       class="eb_label expanded">
                                                                    <input type="text"
                                                                           name="billing_postcode"
                                                                           id="billing_postcode"
                                                                           aria-label="Postcode lookup"
                                                                           placeholder="Postcode*"
                                                                           autocomplete="postal-code" required>
                                                                    <span>Postcode*</span>
                                                                    <a href="#" id="postCodeLookup"
                                                                       class="address_lookup">lookup
                                                                        address</a>
                                                                </label>

                                                                <label for="billing_add_line_one"
                                                                       class="eb_label expanded"><input type="text"
                                                                                                        id="billing_add_line_one"
                                                                                                        autocomplete="street-address"
                                                                                                        placeholder="Address line one*"
                                                                                                        aria-label="Address line one is required"
                                                                                                        required><span>Address line one*</span></label>
                                                                <label for="billing_add_line_two"
                                                                       class="eb_label expanded"><input type="text"
                                                                                                        id="billing_add_line_two"
                                                                                                        autocomplete="address-level2"
                                                                                                        placeholder="Address line two*"
                                                                                                        aria-label="Address line two is required"
                                                                                                        required><span>Address line two*</span></label>
                                                                <label for="billing_town"
                                                                       class="eb_label"><input type="text"
                                                                                               id="billing_town"
                                                                                               autocomplete="city"
                                                                                               aria-label="Town"
                                                                                               placeholder="Town"><span>Town</span></label>
                                                                <label for="billing_county"
                                                                       class="eb_label"><input type="text"
                                                                                               id="billing_county"
                                                                                               autocomplete="county"
                                                                                               aria-label="County"
                                                                                               placeholder="County"><span>County</span></label>


                                                            </div>

                                                            <label for="billing_pay_by_card"
                                                                   class="eb_checkbox large">
                                                                <input type="radio" name="billing_payment_method"
                                                                       id="billing_pay_by_card">
                                                                <span>Pay by card</span>
                                                            </label>

                                                            <!--                                                            <div class="card_payment">
                                                                                                                                <label for="billing_cc_number" class="eb_label expanded">
                                                                                                                                    <input type="text" placeholder="Card Number" autocomplete="cc-number" aria-label="Card number" id="billing_cc_number">
                                                                                                                                    <span>Card Number</span>
                                                                                                                                </label>

                                                                                                                                <label for="billing_cc_exp_mo" class="eb_label">
                                                                                                                                    <input type="text" placeholder="Month" autocomplete="cc-exp-month" aria-label="Expiry month" id="billing_cc_exp_mo">
                                                                                                                                    <span>Month</span>
                                                                                                                                </label>

                                                                                                                                <label for="billing_cc_exp_year" class="eb_label">
                                                                                                                                    <input type="text" placeholder="Month" autocomplete="cc-exp-year" aria-label="Expiry month" id="billing_cc_exp_year">
                                                                                                                                    <span>Year</span>
                                                                                                                                </label>

                                                                                                                                <label for="billing_cc_exp_cvv" class="eb_label">
                                                                                                                                    <input type="text" placeholder="CVV" autocomplete="cc-exp-csc" aria-label="Expiry month" id="billing_cc_exp_cvv">
                                                                                                                                    <span>CVV</span>
                                                                                                                                </label>
                                                                                                                                <p class="help-text">Last 3 digits on the back of your card</p>
                                                                                                                        </div>-->

                                                            <label for="billing_pay_by_invoice"
                                                                   class="eb_checkbox large">
                                                                <input type="radio" name="billing_payment_method"
                                                                       id="billing_pay_by_invoice">
                                                                <span>Pay by invoice</span>
                                                            </label>

                                                            <label for="gdpr_1"
                                                                   class="eb_checkbox small">
                                                                <input type="checkbox"
                                                                       id="gdpr_1">
                                                                <span>I hereby agree to the Terms and Conditions of registering for a training
                                                            course with Easybook Training Ltd</span>
                                                            </label>
                                                            <label for="gdpr_2"
                                                                   class="eb_checkbox small">
                                                                <input type="checkbox"
                                                                       id="gdpr_2">
                                                                <span>I have read the course prerequisite(s). <a
                                                                            href="#" target="_blank">Course Prerequisites</a><br><br>
                                                                Delegates must be in possession of a current First Aid at Work or a First Aid at Work Refresher Certificate. If certification has expired the delegate is no longer considered to be a qualified First Aider.
                                                                </span>
                                                            </label>
                                                            <label for="gdpr_3"
                                                                   class="eb_checkbox small">
                                                                <input type="checkbox"
                                                                       id="gdpr_3">
                                                                <span>I would like to hear from Easybook Training with any news or offers & agree to further communication.</span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="checkout-stage-tab-footer">
                                                    <a href="" class="button eb_btn"
                                                       data-stage-continue="orderConfirmation">Next</a>
                                                </div>
                                            </div>

                                            <div class="checkout-stage-tab" id="orderConfirmation">
                                                <div class="checkout-stage-tab-content">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="cell small-12 large-5" data-sticky-container>
                                            <div class="sticky order-summary-sticky" data-sticky
                                                 data-anchor="sticker" data-top-anchor="sticker:top"
                                                 data-btm-anchor="sticker:bottom" data-sticky-on="large"
                                                 data-check-every="0" data-margin-top="1">
                                                <div class="order-summary-wrap">
                                                    <div class="order-summary">
                                                        <div class="order-summary-header">
                                                            <span class="order-summary-name">Summary</span>
                                                        </div>
                                                        <div class="order-summary-main">
                                                            <div class="order-summary-content">
                                                                <div class="info">Price per person <span class="site_currency">&pound;</span><span id="bookinValue">250</span> <sup>ex VAT</sup></div>
                                                                    <div class="inline_input">
                                                                        <label for="numberOfDelegates">No. of delegates</label>
                                                                        <div class="custom_select shade">
                                                                            <select name="" id="numberOfDelegates">
                                                                                <option value="1">1</option>
                                                                                <option value="2">2</option>
                                                                                <option value="3" selected>3</option>
                                                                                <option value="4">4</option>
                                                                                <option value="5">5</option>
                                                                                <option value="6">6</option>
                                                                                <option value="7">7</option>
                                                                                <option value="8">8</option>
                                                                                <option value="9">9</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                <?php for ($i = 0; $i < 1; $i++) : ?>
                                                                    <div class="order-summary-details">
                                                                        <div class="order-summary-head">
                                                                            <div class="name">Course
                                                                                Name <?php echo $i ?></div>
  <!--                                                                          <div class="value"><span
                                                                                        class="site_currency">&pound;</span>250
                                                                            </div>-->
                                                                        </div>
                                                                        <div class="order-summary-detail"><span
                                                                                    class="fa fa-calendar"></span>01 Jan
                                                                            19 - 03 Jan 19
                                                                        </div>
                                                                        <div class="order-summary-detail"><span
                                                                                    class="fa fa-clock-o"></span>08:00 -
                                                                            17:00
                                                                        </div>
                                                                        <div class="order-summary-detail"><span
                                                                                    class="fa fa-map-marker"></span>Dartford.
                                                                            DA1 1QZ
                                                                        </div>
                                                                    </div>
                                                                <?php endfor; ?>
                                                            </div>
                                                        </div>
                                                        <div class="order-summary-footer">
                                                            <div class="order-total">
                                                                <div class="name">Subtotal:</div>
                                                                <div class="value"><span
                                                                            class="site_currency">&pound;</span>750.00
                                                                </div>
                                                            </div>
                                                            <div class="order-total">
                                                                <div class="name">VAT at <span class="vat_local"
                                                                                               id="vatLocal">20%</span>
                                                                </div>
                                                                <div class="value"><span
                                                                            class="site_currency">&pound;</span>150
                                                                </div>
                                                            </div>
                                                            <div class="order-total grand-total">
                                                                <div class="name">total</div>
                                                                <div class="value"><span
                                                                            class="site_currency">&pound;</span>900
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="payment_methods_accepted">
                                                    <img src="../assets/img/payment_methods_accepted.png" alt="">
                                                    <img src="../assets/img/logo.png" alt="">
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </form>
        <div class="order_confirmation up-down just-down" style="display: none">
            <div class="grid-container">
                <div class="grid-x grid-padding-x align-center">
                    <div class="cell large-10 small-12">
                        <div class="table-scroll">
                            <table class="checkout_table">
                                <thead>
                                <tr>
                                    <th>Course</th>
                                    <th colspan="0">Booking Ref.</th>
                                    <th colspan="0">No. of People</th>
                                    <th colspan="0">Price <sup>ex VAT</sup></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($i = 0; $i < 3; $i++) : ?>
                                    <tr>
                                        <td>
                                            <div class="order-summary-details">
                                                <div class="order-summary-head">
                                                    <div class="name">Course
                                                        Name 0
                                                    </div>
                                                </div>
                                                <div class="order-summary-detail"><span class="fa fa-calendar"></span>01
                                                    Jan
                                                    19 - 03 Jan 19
                                                </div>
                                                <div class="order-summary-detail"><span class="fa fa-clock-o"></span>08:00
                                                    -
                                                    17:00
                                                </div>
                                                <div class="order-summary-detail"><span class="fa fa-map-marker"></span>Dartford.
                                                    DA1 1QZ
                                                </div>
                                            </div>
                                        </td>
                                        <td colspan="0"><span class="booking_ref"><?php echo 7341850328 + $i ?></span>
                                        </td>
                                        <td colspan="0"><span class="no_of_people">1</span></td>
                                        <td colspan="0"><span class="course_price"><span class="site_currency">&pound;</span>250<sup>pp</sup></span></td>
                                    </tr>
                                <?php endfor; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="cell large-10 small-12">
                        <div class="grid-x grid-padding-x align-bottom">
                            <div class="cell large-9 small-12">
                                <div class="eb_cms_content formatted">
                                    <h5>What’s next?</h5>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                        consequat.</p>

                                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                        culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde
                                        omnis.</p>
                                </div>
                            </div>
                            <div class="cell large-3 small-12">
                                <a href="#" class="button eb_btn small">Print/Save Receipt</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php include('../template/footer.php'); ?>