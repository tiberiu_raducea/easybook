<?php include('../template/header-checkout.php'); ?>

    <section class="eb_login_area text-center large-text-left is_hidden <?php if ($status) : ?>logged_in<?php endif; ?>" data-toggler="is_hidden"
             id="logInArea">
        <div id="logInAreaContent" class="eb_login_area_content hide_login" data-toggler="hide_login">
            <?php if ($status) : ?>
                <div class="grid-container">
                    <div class="grid-x grid-padding-x align-center">
                        <div class="cell small-12 large-10">
                            <div class="grid-x grid-padding-x align-middle">
                                <div class="cell large-auto">
                                    <div class="user_greeting">
                                        <h3><span class="fa fa-user"></span> Welcome back, David Jones</h3>
                                    </div>
                                </div>
                                <div class="cell large-shrink">
                                    <div class="user_cta">
                                        <a href="" class="edit_details">Edit Details</a>
                                        <a href="" class="button eb_btn shrunk">Log out</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>


                <div class="grid-container">
                    <div class="grid-x grid-padding-x align-center">
                        <div class="cell small-12 large-10">
                            <div class="grid-x grid-padding-x">
                                <div class="cell small-12 large-9">
                                    <form action="" class="eb_login_form">
                                        <div class="grid-x grid-padding-x  align-middle">
                                            <div class="cell large-shrink">
                                                <label for="logUserEmail" class="eb_login_form_headline">
                                                    <span class="fa fa-user"></span>
                                                    Log in
                                                </label>
                                            </div>
                                            <div class="cell large-auto">
                                                <label class="eb_label invert">
                                                    <input type="email" id="logUserEmail" aria-label="Email Address"
                                                           autocomplete="user-name"
                                                           placeholder="Email Address" required="">
                                                    <span>Email Address</span>
                                                </label>
                                            </div>
                                            <div class="cell large-auto">
                                                <label for="" class="eb_label invert">
                                                    <input type="password" placeholder="Password (case sensitive)"
                                                           aria-label="case sensitive password"
                                                           autocomplete="current-password"
                                                           required=""
                                                           aria-describedby="passwordHelpText">
                                                    <span>Password (case sensitive)</span>
                                                </label>
                                                <p class="help-text" id="passwordHelpText"><a href="#">I've forgotten my
                                                        password</a></p>
                                            </div>
                                            <div class="cell large-shrink">
                                                <button type="submit" class="button eb_btn" aria-label="Log in">Log in
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="cell small-12 large-3">
                                    <a href="#" class="eb_create_account">
                                        <span>Don't have an EasyBook login?</span>
                                        <span>Create an account</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <button class="close-button " data-toggle="logInAreaContent">&times;</button>
        </div>
        <button class="button eb_btn shrunk toggle_login" data-toggle="logInAreaContent">LOGIN</button>
    </section>

    <section class="eb_content_area">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope=""
                            itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="eb_basket_area">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-12 large-10">
                    <div class="eb_headline_block fluid text-center just-down">
                        <h4 class="eb_headline">Basket Summary</h4>
                    </div>
                </div>

                <div class="cell small-12 large-10">

                    <div class="grid-x grid-padding-x">
                        <div class="cell large-8 small-12">
                            <div class="table-scroll">
                                <table class="basket_table">
                                    <thead>
                                    <tr>
                                        <th><span class="name">Course</span></th>
                                        <th colspan="0"><span class="name mobile_hide">No. of People</span></th>
                                        <th colspan="0"><span class="name">Price <sup>ex VAT</sup></span></th>
                                        <th colspan="0"><span class="name"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php for ($i = 0; $i < 3; $i++) : ?>
                                        <tr id="itemRow<?php echo $i ?>">
                                            <td>
                                                <div class="order-summary-details">
                                                    <div class="order-summary-head">
                                                        <div class="name">Course
                                                            Name 0
                                                        </div>
                                                    </div>
                                                    <div class="order-summary-detail"><span
                                                                class="fa fa-calendar"></span>01
                                                        Jan
                                                        19 - 03 Jan 19
                                                    </div>
                                                    <div class="order-summary-detail"><span
                                                                class="fa fa-clock-o"></span>08:00
                                                        -
                                                        17:00
                                                    </div>
                                                    <div class="order-summary-detail"><span
                                                                class="fa fa-map-marker"></span>Dartford.
                                                        DA1 1QZ
                                                    </div>
                                                </div>
                                            </td>
                                            <td colspan="0">
                                                <div class="custom_select shade">
                                                    <label for="numberOfPeople<?php echo $i ?>" class="update_item">
                                                        <select name="" id="numberOfPeople<?php echo $i ?>">
                                                            <option value="1">1</option>
                                                            <option value="2" selected>2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                        </select>
                                                    </label>

                                                </div>
                                            </td>
                                            <td colspan="0"><span class="course_price"><span class="site_currency">&pound;</span>250<sup>pp</sup></span>
                                            </td>
                                            <td colspan="0">
                                                <button role="button" class="remove_item"><span
                                                            class="fa fa-times"></span><span>Remove</span></button>
                                            </td>
                                        </tr>
                                    <?php endfor; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="cell large-4 small-12">
                            <div class="order-summary-wrap">
                                <div class="order-summary">
                                    <div class="order-summary-header">
                                        <span class="order-summary-name">Summary</span>
                                    </div>
                                    <div class="order-summary-footer">
                                        <div class="order-total">
                                            <div class="name">Subtotal:</div>
                                            <div class="value"><span
                                                        class="site_currency">&pound;</span>750.00
                                            </div>
                                        </div>
                                        <div class="order-total">
                                            <div class="name">VAT at <span class="vat_local"
                                                                           id="vatLocal">20%</span>
                                            </div>
                                            <div class="value"><span
                                                        class="site_currency">&pound;</span>150
                                            </div>
                                        </div>
                                        <div class="order-total grand-total">
                                            <div class="name">total</div>
                                            <div class="value"><span
                                                        class="site_currency">&pound;</span>900
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="payment_methods_accepted centered">
                                <img src="../assets/img/payment_methods_accepted.png" alt="">
                                <img src="../assets/img/logo.png" alt="">
                            </div>

                            <div class="user_direction">
                                <a href="#" class="continue">Continue Shopping</a>
                                <a href="#" class="button eb_btn">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include('../template/footer.php'); ?>