<!--About us page-->

<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Why Choose Us?</h4>
                    <div class="eb_headline_sub strong">

                        <p>Here at Easybook Training we want to make finding your perfect <a href="#">training course</a> as simple as
                            possible.</p>

                        <p>We’re available seven days a week, and are happy to assist with your enquiry whenever best
                            suits you. As a family-run business we are proud to provide a professional service, with a
                            personal touch.</p>
                    </div>
                </div>
            </div>

            <div class="cell small-12 large-10">
                <!-- Child divs have clear in css so they don't render odd text after images-->
                <div class="post_article_content">
                    <div class="text-left">
                        <img src="//picsum.photos/470/260/" alt="" class="img-right">
                        <p>Mel eu diceret vocibus, has oratio delicata instructior eu, sit id impetus consulatu
                            omittantur.
                            Omnis euismod dignissim in mei, porro minimum nominati est ut. Eos autem tacimates electram
                            ea,
                            ne aperiri euismod sea. At omnis diceret quo, ea amet enim debitis vis, sea sanctus
                            explicari
                            suscipiantur et. Erat numquam dolorum te mel, ad sit dicta facilis explicari.</p>

                        <p>Ne amet alii munere mea. Nulla accommodare necessitatibus ei pri, deserunt periculis
                            evertitur ut
                            quo. Solet putent fuisset et vel, case democritum incorrupte eam in, mei no oblique iuvaret.
                            Debet impedit gubergren id nec. Appetere explicari adolescens ne mel, modo corpora commune
                            sed
                            in. Ad modus utinam usu, eum ad doctus impetus integre. Assum inani menandri te vim, his
                            detraxit erroribus et.</p>
                    </div>

                    <div class="text-right">
                        <img src="//picsum.photos/470/260/" alt="" class="img-left">
                        <p>Mel eu diceret vocibus, has oratio delicata instructior eu, sit id impetus consulatu
                            omittantur.
                            Omnis euismod dignissim in mei, porro minimum nominati est ut. Eos autem tacimates electram
                            ea,
                            ne aperiri euismod sea. At omnis diceret quo, ea amet enim debitis vis, sea sanctus
                            explicari
                            suscipiantur et. Erat numquam dolorum te mel, ad sit dicta facilis explicari.</p>

                        <p>Ne amet alii munere mea. Nulla accommodare necessitatibus ei pri, deserunt periculis
                            evertitur ut
                            quo. Solet putent fuisset et vel, case democritum incorrupte eam in, mei no oblique iuvaret.
                            Debet impedit gubergren id nec. Appetere explicari adolescens ne mel, modo corpora commune
                            sed
                            in. Ad modus utinam usu, eum ad doctus impetus integre. Assum inani menandri te vim, his
                            detraxit erroribus et.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <div class="eb_headline_block text-center">
                    <h4 class="eb_headline">Lorem Ipsum dolor sit amet</h4>
                    <div class="eb_headline_secondary">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">


        <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-4">
            <?php for ($i = 1; $i < 5; $i++) : ?>
                <div class="cell">
                    <div class="eb_usp_box" tabindex="0">
                        <div class="eb_usp_image">
                            <img src="<?php echo $site_url; ?>assets/img/eb_icon_<?php echo $i; ?>.png"
                                 alt="UK Nationwide Coverage <?php echo $i; ?>">
                        </div>
                        <div class="eb_usp_title">UK Nationwide Coverage <?php echo $i; ?></div>
                        <div class="eb_usp_text">From Aberdeen to Isle of Wight</div>
                        <!-- Link is optional-->
                        <a href="#" class="eb_usp_link" title="UK Nationwide Coverage <?php echo $i; ?>"
                           aria-label="UK Nationwide Coverage">UK Nationwide Coverage <?php echo $i; ?></a>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10">
                <div class="eb_content_block text-center">
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu fugiat nulla pariatur.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
            </div>
        </div>
    </div>

</section>

<?php include('../template/footer.php'); ?>