<?php

include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

    <section class="eb_content_area">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell large-10 small-12">
                    <div class="eb_headline_block fluid text-center">
                        <h4 class="eb_headline">Lorem ipsum dolor sit amet</h4>
                        <div class="eb_headline_sub">
                            <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu
                                vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes
                                ius, harum percipitur intellegebat sea eu, ius ut oratio latine rationibus. In audiam
                                tincidunt mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="eb_content_area up-down just-down">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-12 large-10">
                    <ul class="accordion" data-accordion>
                        <?php for ($i = 0; $i < 10; $i++) : ?>
                            <li class="accordion-item <?php echo ($i === 0) ? 'is-active' : '' ?>" data-accordion-item>
                                <a href="#" class="accordion-title">Lorem ipsum dolor sit amet, te sea partiendo
                                    suavitate?</a>
                                <div class="accordion-content" data-tab-content>
                                    <p><?php echo $i ?> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                                        eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                        culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                        culpa qui officia deserunt mollit anim id est laborum.</p>

                                    <div class="text-right">
                                        <a href="" class="button eb_btn">Read more</a>
                                    </div>

                                </div>
                            </li>
                        <?php endfor; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>


<?php include('../template/footer.php'); ?>