<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area up-down small just-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">

                    <h4 class="eb_headline">Thank You</h4>
                    <div class="eb_headline_sub">

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat.</p>

                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="eb_contact_area">
    <div class="eb_contact_background" style="background-image: url(/assets/img/eb_contact_background.jpg)"></div>
    <div class="eb_contact_content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <form action="/" class="eb_contact_form" enctype="multipart/form-data" autocomplete="on">
                        <div class="eb_contact_header">
                            <h2 class="title">We’ll call you</h2>
                            <p class="text">Simply leave your contact information below and we’ll call you back as
                                soon as one of our team become available.</p>
                        </div>
                        <div class="grid-x grid-padding-x">
                            <div class="cell large-10">
                                <div class="eb_contact_form_items">
                                    <label for="f_name" class="eb_label invert">
                                        <input type="text" name="f_name" id="f_name" aria-label="First Name"
                                               placeholder="First Name" autocomplete="given-name" required>
                                        <span>First Name</span>
                                    </label>

                                    <label for="l_name" class="eb_label invert">
                                        <input type="text" name="l_name" id="l_name" aria-label="Last Name"
                                               placeholder="Last Name" autocomplete="family-name" required>
                                        <span>Last Name</span>
                                    </label>

                                    <label for="phone_no" class="eb_label invert expanded">
                                        <input type="tel" name="phone_no" id="phone_no" aria-label="Phone Number"
                                               placeholder="Phone Number" autocomplete="tel" required>
                                        <span>Phone Number</span>
                                    </label>

                                    <label for="email_add" class="eb_label invert expanded">
                                        <input type="email" name="email_add" id="email_add" aria-label="Email Address"
                                               placeholder="Email Address" autocomplete="email" required>
                                        <span>Email Address</span>
                                    </label>

                                    <label for="customer_course" class="eb_label invert expanded custom_select">
                                        <select name="customer_course" id="customer_course">
                                            <option value="" selected hidden>Which course are you interested in?
                                            </option>
                                            <option value="Lorem ipsum">Lorem ipsum</option>
                                            <option value="Dolor sit amet">Dolor sit amet</option>
                                            <option value="Consectetuer adipiscing elit">Consectetuer adipiscing elit
                                            </option>
                                            <option value="Aenean commodo">Aenean commodo</option>
                                            <option value="Ligula eget dolor">Ligula eget dolor</option>
                                        </select>
                                        <span>Which course are you interested in?</span>
                                    </label>

                                    <label for="gdpr_1" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_1">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I agree that my data will be used and stored as outlined in the <a
                                                    href="" target="_blank">Terms and Conditions</a> on the Easybook Training website
                                            </span>
                                    </label>

                                    <label for="gdpr_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_2">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I am happy to receive newsletters and promotional information from Easybook Training Ltd
                                            </span>
                                    </label>

                                    <div class="eb_contact_form_footer text-center">
                                        <button type="submit" class="button eb_btn" value="submit" tabindex="0"
                                                aria-label="Submit form">Submit
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 large-6"></div>
            </div>
        </div>
    </div>
</section>
<?php include('../template/footer.php'); ?>
