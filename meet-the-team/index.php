<!-- Meet the team page-->

<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Key people</h4>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut orav rationibus. In audiam tincidunt mel. Vim
                            ad adhuc augue, eos lorem velit decore in.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2">
                    <?php for ($i = 0; $i < 10; $i++) : ?>
                        <div class="cell">
                            <div class="eb_v_card">
                                <a href="#" class="eb_v_card_thumb"
                                   style="background-image: url(//picsum.photos/300/300);">
                                    <span>find out more</span>
                                </a>
                                <div class="eb_v_card_detail">
                                    <div class="info">
                                        <div class="title"><a href="#">Jane Smith</a></div>
                                        <div class="role">Job Title here</div>

                                        <div class="contact">
                                            <div class="contact_method">
                                                <span class="fa fa-phone"></span> Phone:
                                            </div>
                                            <a href="tel:08000 931 189">08000 931 189</a>
                                        </div>

                                        <div class="contact">
                                            <div class="contact_method">
                                                <span class="fa fa-envelope"></span> Email:
                                            </div>
                                            <a href="mailto:jane@easybooktraining.com">jane@easybooktraining.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>

        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut orav rationibus. In audiam tincidunt mel. Vim
                            ad adhuc augue, eos lorem velit decore in.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('../template/footer.php'); ?>