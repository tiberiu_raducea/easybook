<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x">
                    <div class="cell small-12 medium-6">
                        <img src="//picsum.photos/575" alt="" class="img-center">
                    </div>
                    <div class="cell small-12 medium-6">
                        <div class="team_detail">
                            <div class="info">
                                <div class="title"><a href="#">Jane Smith</a></div>
                                <div class="role">Job Title here</div>

                                <div class="contact">
                                    <div class="contact_method">
                                        <span class="fa fa-phone"></span> Phone:
                                    </div>
                                    <a href="tel:08000 931 189">08000 931 189</a>
                                </div>

                                <div class="contact">
                                    <div class="contact_method">
                                        <span class="fa fa-envelope"></span> Email:
                                    </div>
                                    <a href="mailto:jane@easybooktraining.com">jane@easybooktraining.com</a>
                                </div>

                            </div>

                            <div class="eb_cms_content formatted up-down small just-up">

                                <h4>Experience</h4>

                                <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd
                                    usu vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul
                                    principes ius, harum percipitur intellegebat sea eu, ius ut orav rationibus. In
                                    audiam tincidunt mel. Vim ad adhuc augue, eos lorem velit decore in.</p>

                                <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd
                                    usu vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul
                                    principes ius, harum percipitur intellegebat sea eu, ius ut orav rationibus. In
                                    audiam tincidunt mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="fun_fact_area up-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x grid-padding-y align-middle">
                    <div class="cell medium-shrink">
                        <div class="fun_fact_title">Fun fact about Jane...</div>
                    </div>
                    <div class="cell medium-auto">
                        <div class="fun_fact_text">Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero
                            option. EtiamAd usu vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area up-down back_white">
    <div class="feedback-wrap">
        <div class="feedback">
            <div class="feedback-slider">
                <?php for ($i = 0; $i < 10; $i++): ?>
                    <div class="feedback-slider-item">
                        <div class="feedback-element">
                            <div class="title">So simple, will 100% book again through EasyBook training</div>
                            <div class="quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                                ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                Nulla consequat
                                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                            </div>
                            <div class="author">Darren Rogan</div>
                            <div class="author-comp">Rogan Industries</div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>

<section class="eb_contact_area up-down-out just-up">
    <div class="eb_contact_background" style="background-image: url(../assets/img/eb_contact_background.jpg)"></div>
    <div class="eb_contact_content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <form action="/" class="eb_contact_form" enctype="multipart/form-data" autocomplete="on">

                        <div class="grid-x grid-padding-x">
                            <div class="cell large-10">
                                <div class="eb_contact_header">
                                    <h2 class="title">Get in touch</h2>
                                    <p class="text">Simply leave your contact information below and we’ll call you back
                                        as
                                        soon as one of our team members becomes available.</p>
                                </div>

                                <div class="eb_contact_form_items">
                                    <label for="f_name" class="eb_label invert">
                                        <input type="text" name="f_name" id="f_name" aria-label="First Name"
                                               placeholder="First Name" autocomplete="given-name" required>
                                        <span>First Name</span>
                                    </label>

                                    <label for="l_name" class="eb_label invert">
                                        <input type="text" name="l_name" id="l_name" aria-label="Last Name"
                                               placeholder="Last Name" autocomplete="family-name" required>
                                        <span>Last Name</span>
                                    </label>

                                    <label for="email_add" class="eb_label invert expanded">
                                        <input type="email" name="email_add" id="email_add" aria-label="Email Address"
                                               placeholder="Email Address" autocomplete="email" required>
                                        <span>Email Address</span>
                                    </label>

                                    <label for="phone_no" class="eb_label invert expanded">
                                        <input type="tel" name="phone_no" id="phone_no" aria-label="Phone Number"
                                               placeholder="Phone Number" autocomplete="tel" required>
                                        <span>Phone Number</span>
                                    </label>

                                    <label for="client_message" class="eb_label invert expanded eb_textarea">
                                        <textarea name="message" id="client_message" cols="30" rows="5"></textarea>
                                        <span>Message</span>
                                    </label>

                                    <label for="gdpr_1" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_1">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I agree that my data will be used and stored as outlined in the <a
                                                    href="" target="_blank">Terms and Conditions</a> on the Easybook Training website
                                            </span>
                                    </label>

                                    <label for="gdpr_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_2">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I am happy to receive newsletters and promotional information from Easybook Training Ltd
                                            </span>
                                    </label>

                                    <div class="eb_contact_form_footer text-center">
                                        <button type="submit" class="button eb_btn" value="submit" tabindex="0"
                                                aria-label="Submit form">Submit
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 large-6"></div>
            </div>
        </div>
    </div>
</section>

<?php include('../template/footer.php'); ?>
