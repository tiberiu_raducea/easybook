<?php include('../template/header.php'); ?>


<?php include('../template/search.php'); ?>

    <section class="eb_content_area">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope=""
                            itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="eb_content_area">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell large-10 small-12">
                    <div class="grid-x grid-padding-x align-middle align-center">
                        <div class="cell shrink order-1">
                            <img src="/assets/img/providers/company_1.png" alt="">
                        </div>

                        <div class="cell shrink order-2 large-order-3">
                            <img src="/assets/img/providers/company_1_acc.jpg" alt="">
                        </div>

                        <div class="cell large-auto order-3 large-order-2">
                            <div class="eb_headline_block fluid text-center">
                                <h4 class="eb_headline">3B Training</h4>
                                <div class="eb_headline_sub">
                                    <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu
                                        vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes
                                        ius, harum percipitur intellegebat sea eu, ius ut orav rationibus. In audiam tincidunt
                                        mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="eb_content_area up-down just-down">
        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell small-12 large-10">
                    <form action="" class="post_filters">
                        <label for="inputSearch" class="post_search_label">
                            <span class="fa fa-search"></span>
                            <input type="text" placeholder="Type here to search or select below"
                                   name="postSearchInput" id="inputSearch" onkeyup="filterSearch()">
                        </label>
                    </form>
                </div>

                <div class="cell small-12 large-10">
                    <h4 class="column_listing_title">Courses delivered by 3B Training</h4>
                    <div class="column_listing">
                        <ul id="searchUnordered">
                            <li><a href="#" aria-label="Abrasive Wheels Course" title="Abrasive Wheels Course">Abrasive Wheels Course</a></li>
                            <li><a href="#" aria-label="Asbestos Awareness Course" title="Asbestos Awareness Course">Asbestos Awareness Course</a></li>
                            <li><a href="#" aria-label="CITB Health & Safety Awareness Course" title="CITB Health & Safety Awareness Course">CITB Health & Safety Awareness Course</a></li>
                            <li><a href="#" aria-label="CITB Site Environmental Awareness SEATS Course" title="CITB Site Environmental Awareness SEATS Course">CITB Site Environmental Awareness SEATS Course</a></li>
                            <li><a href="#" aria-label="CITB SMSTS 5 Day Course" title="CITB SMSTS 5 Day Course">CITB SMSTS 5 Day Course</a></li>
                            <li><a href="#" aria-label="CITB SMSTS Refresher 2 Day Course" title="CITB SMSTS Refresher 2 Day Course">CITB SMSTS Refresher 2 Day Course</a></li>
                            <li><a href="#" aria-label="CITB SSSTS 2 Day Course" title="CITB SSSTS 2 Day Course">CITB SSSTS 2 Day Course</a></li>
                            <li><a href="#" aria-label="CITB SSSTS Refresher 1 Day Course" title="CITB SSSTS Refresher 1 Day Course">CITB SSSTS Refresher 1 Day Course</a></li>
                            <li><a href="#" aria-label="CITB Temporary Works Co-ordinator (TWCTC) Course" title="CITB Temporary Works Co-ordinator (TWCTC) Course">CITB Temporary Works Co-ordinator (TWCTC) Course</a></li>
                            <li><a href="#" aria-label="Emergency First Aid at Work Course" title="Emergency First Aid at Work Course">Emergency First Aid at Work Course</a></li>
                            <li><a href="#" aria-label="Emergency First Aid at Work Course" title="Emergency First Aid at Work Course">Emergency First Aid at Work Course</a></li>
                            <li><a href="#" aria-label="Fire Marshal Course" title="Fire Marshal Course">Fire Marshal Course</a></li>
                            <li><a href="#" aria-label="First Aid at Work Course" title="First Aid at Work Course">First Aid at Work Course</a></li>
                            <li><a href="#" aria-label="IOSH Managing Safely Course" title="IOSH Managing Safely Course">IOSH Managing Safely Course</a></li>
                            <li><a href="#" aria-label="IOSH Managing Safely Refresher Course" title="IOSH Managing Safely Refresher Course">IOSH Managing Safely Refresher Course</a></li>
                            <li><a href="#" aria-label="IOSH Working Safely Course" title="IOSH Working Safely Course">IOSH Working Safely Course</a></li>
                            <li><a href="#" aria-label="NEBOSH Construction Certificate" title="NEBOSH Construction Certificate">NEBOSH Construction Certificate</a></li>
                            <li><a href="#" aria-label="NEBOSH General Certificate" title="NEBOSH General Certificate">NEBOSH General Certificate</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php include('../template/footer.php'); ?>