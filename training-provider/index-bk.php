<?php include('../template/header.php'); ?>
<section class="eb_internal_search">
    <form action="" class="eb_internal_search_form">
        <div class="grid-container fluid">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <div class="grid-x grid-padding-x">
                        <div class="cell medium-6">
                            <label for="selectCategory" class="eb_select">
                                <select name="search_category" id="selectCategory">
                                    <option value="" selected hidden>Select a category</option>
                                    <option value="Category 1">Category 1</option>
                                    <option value="Category 2">Category 2</option>
                                    <option value="Category 3">Category 3</option>
                                    <option value="Category 4">Category 4</option>
                                    <option value="Category 5">Category 5</option>
                                </select>
                            </label>
                        </div>
                        <div class="cell medium-6">
                            <label for="selectCourseTitle" class="eb_select">
                                <select name="search_category" id="selectCourseTitle">
                                    <option value="" selected hidden>Select a course title</option>
                                    <option value="Course Title 1">Course Title 1</option>
                                    <option value="Course Title 2">Course Title 2</option>
                                    <option value="Course Title 3">Long Course Title that goes on two
                                        lines on some screens
                                    </option>
                                    <option value="Course Title 4">Even longer this Course Title 4
                                    </option>
                                    <option value="Course Title 5">Some long Course Title 5</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="cell small-12 large-6">
                    <div class="grid-x grid-padding-x">
                        <div class="cell medium-shrink">
                            <label for="ebPostcode" class="eb_clean_input post_code">
                                <input type="text" id="ebPostcode" placeholder="Postcode">
                            </label>
                        </div>
                        <div class="cell medium-auto">
                            <label for="ebDates" class="eb_clean_input edge">
                                <span class="fa fa-calendar" aria-hidden="true"></span>
                                <input type="text" id="ebDates" name="daterange"
                                       placeholder="Start between">
                            </label>
                        </div>
                        <div class="cell large-shrink">
                            <button type="submit" value="searchEasyBook" class="button eb_btn">Search</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</section>
<section class="eb_content_area up-down small just-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Become a Training Provider</h4>
                    <div class="eb_headline_sub">
                        <p>At Easybook Training we work in partnership with hundreds of training providers across
                            the UK.</p>

                        <p>We're very proud of our network of accredited and highly skilled safety training
                            providers as it means we are able to offer a rich variety course in all major locations
                            across the UK:</p>
                    </div>
                </div>
            </div>

            <div class="cell large-10 small-12">
                <div class="grid-x grid-padding-x grid-padding-y">
                    <div class="cell large-auto">
                        <div class="eb_cms_content formatted">
                            <ul>
                                <li><a href="">Health and Safety training</a> with IOSH, CITB, NEBOSH, NRSWA, PASMA,
                                    IPAF and
                                    ECITB
                                </li>
                                <li><a href="">First Aid courses, refreshers and paediatric training</a></li>
                                <li><a href="">Fire Safety training</a> including Fire Certiificate and Fire Marshal
                                    courses
                                </li>
                                <li><a href="">Working at Height</a> including PASMA, IPAF, CISRS and Ladder Association
                                    courses
                                </li>
                                <li><a href="">Food Safety training</a> Level 1, Level 2, Level 3 and Level 4, APLH and
                                    SCPLH
                                    course
                                </li>
                                <li><a href="">Construction training and refresher courses</a></li>
                            </ul>
                            <p>We value our relationships with our partners very highly, whether you are an already
                                established name or just starting out, we would be delighted to hear from you.</p>

                        </div>
                    </div>
                    <div class="cell large-shrink">
                        <figure>
                            <img src="//picsum.photos/480/260" alt="">
                            <figcaption></figcaption>
                        </figure>
                    </div>
                </div>
            </div>

            <div class="cell large-10 small-12 text-center">
                <a href="#startTraining" class="button eb_btn" data-smooth-scroll>get in touch</a>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area up-down back_white">
    <div class="feedback-wrap">
        <div class="feedback">
            <div class="feedback-slider">
                <?php for ($i = 0; $i < 10; $i++): ?>
                    <div class="feedback-slider-item">
                        <div class="feedback-element">
                            <div class="title">So simple, will 100% book again through EasyBook training</div>
                            <div class="quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                                ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                Nulla consequat
                                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                            </div>
                            <div class="author">Darren Rogan</div>
                            <div class="author-comp">Rogan Industries</div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area up-down small just-up">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Working with our Providers</h4>
                    <div class="eb_headline_sub">
                        <p>We are able to provide each of our Training Providers with their own unique log-in to our
                            website, meaning they are free to update their training course dates and availability as
                            needed.</p>

                        <p>If you are interested in becoming part of our team, please complete the form opposite and one
                            of
                            our advisers will be in touch.</p>

                        <p><a href="#">View our network of UK training venues here.</a></p>
                    </div>
                </div>
            </div>

            <div class="cell large-10 small-12">
                <div class="eb_headline_block text-center just-down">
                    <h4 class="eb_headline">Why become a provider?</h4>
                    <div class="eb_headline_secondary">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-4">
            <?php for ($i = 1; $i < 5; $i++) : ?>
                <div class="cell">
                    <div class="eb_usp_box" tabindex="0">
                        <div class="eb_usp_image">
                            <img src="<?php echo $site_url; ?>assets/img/eb_icon_<?php echo $i; ?>.png"
                                 alt="UK Nationwide Coverage <?php echo $i; ?>">
                        </div>
                        <div class="eb_usp_title">UK Nationwide Coverage <?php echo $i; ?></div>
                        <div class="eb_usp_text">From Aberdeen to Isle of Wight</div>
                        <!-- Link is optional-->
                        <a href="#" class="eb_usp_link" title="UK Nationwide Coverage <?php echo $i; ?>"
                           aria-label="UK Nationwide Coverage">UK Nationwide Coverage <?php echo $i; ?></a>
                    </div>
                </div>
            <?php endfor; ?>
        </div>

        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <div class="eb_headline_sub">

                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                            commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                            dolore eu fugiat nulla pariatur.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_contact_area" id="startTraining">
    <div class="eb_contact_background"
         style="background-image: url(<?php echo $site_url; ?>assets/img/trainer_bg.jpg)"></div>
    <div class="eb_contact_content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <form action="/" class="eb_contact_form" enctype="multipart/form-data" autocomplete="on">
                        <div class="eb_contact_header">
                            <h2 class="title">We’ll call you</h2>
                            <p class="text">Simply leave your contact information below and we’ll call you back as
                                soon as one of our team become available.</p>
                        </div>
                        <div class="grid-x grid-padding-x">
                            <div class="cell large-10">
                                <div class="eb_contact_form_items">
                                    <label for="f_name" class="eb_label invert">
                                        <input type="text" name="f_name" id="f_name" aria-label="First Name"
                                               placeholder="First Name" autocomplete="given-name" required>
                                        <span>First Name</span>
                                    </label>

                                    <label for="l_name" class="eb_label invert">
                                        <input type="text" name="l_name" id="l_name" aria-label="Last Name"
                                               placeholder="Last Name" autocomplete="family-name" required>
                                        <span>Last Name</span>
                                    </label>

                                    <label for="company" class="eb_label invert expanded">
                                        <input type="text" name="company" id="company" aria-label="Company"
                                               placeholder="Company" autocomplete="tel" required>
                                        <span>Company</span>
                                    </label>

                                    <label for="address" class="eb_label invert expanded">
                                        <input type="text" name="address" id="address" aria-label="Address"
                                               placeholder="Address" autocomplete="email" required>
                                        <span>Address</span>
                                    </label>

                                    <label for="email_add" class="eb_label invert expanded">
                                        <input type="email" name="email_add" id="email_add" aria-label="Email Address"
                                               placeholder="Email Address" autocomplete="email" required>
                                        <span>Email Address</span>
                                    </label>

                                    <label for="phone_no" class="eb_label invert expanded">
                                        <input type="tel" name="phone_no" id="phone_no" aria-label="Phone Number"
                                               placeholder="Phone Number" autocomplete="tel" required>
                                        <span>Phone Number</span>
                                    </label>

                                    <label for="client_message" class="eb_label invert expanded eb_textarea">
                                        <textarea name="message" id="client_message" cols="30" rows="5"></textarea>
                                        <span>Message</span>
                                    </label>

                                    <label for="gdpr_1" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_1">
                                        <span class="gdpr_text">
                                                    <span class="gdpr_tick"></span>
                                                    I agree that my data will be used and stored as outlined in the <a
                                                    href="" target="_blank">Terms and Conditions</a> on the Easybook Training website
                                                </span>
                                    </label>

                                    <label for="gdpr_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_2">
                                        <span class="gdpr_text">
                                                    <span class="gdpr_tick"></span>
                                                    I am happy to receive newsletters and promotional information from Easybook Training Ltd
                                                </span>
                                    </label>

                                    <div class="eb_contact_form_footer text-center">
                                        <button type="submit" class="button eb_btn" value="submit" tabindex="0"
                                                aria-label="Submit form">Submit
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 large-6"></div>
            </div>
        </div>
    </div>
</section>

<?php include('../template/footer.php'); ?>
