<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area up-down small just-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">

                    <h4 class="eb_headline">Training Providers Listing</h4>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                            voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa qui.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x grid-padding-y align-center small-up-1 medium-up-2 large-up-4" data-equalizer data-equalize-by-row="true">
                    <?php for ($i = 0; $i < 12; $i++) : ?>
                        <div class="cell" data-equalizer-watch>
                            <div class="provider" >
                                <div class="provider_detail">
                                    <div class="thumb">
                                        <img src="/assets/img/providers/company_1.png" alt="">
                                    </div>
                                    <div class="title"><?php if ($i == 2): ?>NRSWA (New Roads & Street Works Act) Supervisor Re-Assessment Course<?php endif; ?> 3B Training</div>
                                    <a href="#" class="link" aria-label="<?php if ($i == 2): ?>NRSWA (New Roads & Street Works Act) Supervisor Re-Assessment Course<?php endif; ?> 3B Training"><?php if ($i == 2): ?>NRSWA (New Roads & Street Works Act) Supervisor Re-Assessment Course<?php endif; ?> 3B Training</a>
                                </div>
                                <a href="#" class="button eb_btn">Read more</a>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('../template/footer.php'); ?>
