<?php include('./template/header.php'); ?>

<section class="eb_banner_area">
    <div class="eb_banner_image" style="background-image: url(./assets/img/eb_banner.jpg)"></div>
    <div class="eb_banner_content bottom offset-bottom-2">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell">
                    <div class="eb_search">
                        <h3 class="eb_search_headline">Start your course search here</h3>
                        <form action="" method="" enctype="multipart/form-data" class="eb_search_form">
                            <div class="grid-x grid-padding-y grid-padding-x">
                                <div class="cell small-12 large-7">
                                    <div class="grid-x grid-padding-x">
                                        <div class="cell medium-auto">
                                            <label for="selectCategory" class="eb_select">
                                                <select name="search_category" id="selectCategory">
                                                    <option value="" selected hidden>Select a category</option>
                                                    <option value="Category 1">Category 1</option>
                                                    <option value="Category 2">Category 2</option>
                                                    <option value="Category 3">Category 3</option>
                                                    <option value="Category 4">Category 4</option>
                                                    <option value="Category 5">Category 5</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="cell medium-auto">
                                            <label for="selectCourseTitle" class="eb_select">
                                                <select name="search_category" id="selectCourseTitle">
                                                    <option value="" selected hidden>Select a course title</option>
                                                    <option value="Course Title 1">Course Title 1</option>
                                                    <option value="Course Title 2">Course Title 2</option>
                                                    <option value="Course Title 3">Long Course Title that goes on two
                                                        lines on some screens
                                                    </option>
                                                    <option value="Course Title 4">Even longer this Course Title 4
                                                    </option>
                                                    <option value="Course Title 5">Some long Course Title 5</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell small-12 large-5">
                                    <div class="grid-x grid-padding-x">
                                        <div class="cell medium-auto large-shrink">
                                            <label for="ebPostcode" class="eb_clean_input post_code">
                                                <input type="text" id="ebPostcode" placeholder="Postcode">
                                            </label>
                                        </div>
                                        <div class="cell medium-auto large-auto">
                                            <label for="ebDates" class="eb_clean_input edge">
                                                <span class="fa fa-calendar" aria-hidden="true"></span>
                                                <input type="text" id="ebDates" name="daterange"
                                                       placeholder="Start between">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="grid-x grid-padding-x">
                                <div class="cell text-center">
                                    <button type="submit" value="searchEasyBook" class="button eb_btn">Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <div class="eb_headline_block text-center">
                    <h4 class="eb_headline">or select your course here</h4>
                    <div class="eb_headline_sub">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore
                            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut
                            aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                            velit esse
                            cillum dolore eu <a href="#" role="link">fugiat nulla pariatur</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="grid-container fluid">
        <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-4 align-center course_box_listing">

            <?php $courses = [
                    [
                        'title' => 'Health & Safety',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/health_and_safety.jpg'
                    ],
                    [
                        'title' => 'Construction',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/construction.jpg'
                    ],
                    [
                        'title' => 'First Aid',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/first_aid.jpg'
                    ],
                    [
                        'title' => 'Fire Safety',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/fire_safety.jpg'
                    ],
                    [
                        'title' => 'Plant',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/plant.jpg'
                    ],
                    [
                        'title' => 'In-House',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/in_house.jpg'
                    ],
                    [
                        'title' => 'Working at Height',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/working_at_height.jpg'
                    ],
                    [
                        'title' => 'E-learning',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/e_learning.jpg'
                    ],
                ] ?>
            <?php foreach ($courses as $course) : ?>
                <div class="cell">
                    <div class="course_box" tabindex="0">
                        <div class="course_box_image"
                             style="background-image: url(<?php echo $site_url . $course['image_url'] ?>)"></div>
                        <div class="course_box_content" aria-hidden="true">
                            <div class="course_box_title"><?php echo $course['title'] ?></div>
                            <div class="course_box_text"><?php echo $course['text'] ?></div>
                        </div>
                        <div class="course_box_overlay">
                            <div class="overlay_head">
                                <span><?php echo $course['title'] ?></span>
                                <img src="../assets/img/eg_health_safety_icon.png" alt="">
                            </div>
                            <div class="overlay_body">
                                <dl>
                                    <dt>NEBOSH Training Courses</dt>
                                    <dd>
                                        <ul>
                                            <li>NEBOSH General Certificate</li>
                                            <li>NEBOSH Environmental Certificate</li>
                                        </ul>
                                    </dd>
                                    <dt>IOSH Training Courses</dt>
                                    <dd>
                                        <ul>
                                            <li>IOSH Managing Safely</li>
                                            <li>IOSH Directing Safely</li>
                                        </ul>
                                    </dd>
                                </dl>
                            </div>
                            <div class="overlay_footer">
                                <button class="button eb_btn">view courses</button>
                            </div>
                        </div>
                        <a href="#" class="course_box_link" aria-label="<?php echo $course['title'] ?>"
                           title="<?php echo $course['title'] ?>"><?php echo $course['title'] ?></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <div class="eb_headline_block text-center">
                    <h4 class="eb_headline">Lorem Ipsum dolor sit amet</h4>
                    <div class="eb_headline_secondary">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-4">
            <?php for ($i = 1; $i < 5; $i++) : ?>
                <div class="cell">
                    <div class="eb_usp_box" tabindex="0">
                        <div class="eb_usp_image">
                            <img src="<?php echo $site_url; ?>assets/img/eb_icon_<?php echo $i; ?>.png"
                                 alt="UK Nationwide Coverage <?php echo $i; ?>">
                        </div>
                        <div class="eb_usp_title">UK Nationwide Coverage <?php echo $i; ?></div>
                        <div class="eb_usp_text">From Aberdeen to Isle of Wight</div>
                        <!-- Link is optional-->
                        <a href="#" class="eb_usp_link" title="UK Nationwide Coverage <?php echo $i; ?>"
                           aria-label="UK Nationwide Coverage">UK Nationwide Coverage <?php echo $i; ?></a>
                    </div>
                </div>
            <?php endfor; ?>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10">
                <div class="eb_content_block text-center">
                    <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                        dolore eu fugiat nulla pariatur.</p>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                        voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_contact_area">
    <div class="eb_contact_background" style="background-image: url(./assets/img/eb_contact_background.jpg)"></div>
    <div class="eb_contact_content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <form action="/" class="eb_contact_form" enctype="multipart/form-data" autocomplete="on">
                        <div class="eb_contact_header">
                            <h2 class="title">We’ll call you</h2>
                            <p class="text">Simply leave your contact information below and we’ll call you back as
                                soon as one of our team become available.</p>
                        </div>
                        <div class="grid-x grid-padding-x">
                            <div class="cell large-10">
                                <div class="eb_contact_form_items">
                                    <label for="f_name" class="eb_label invert">
                                        <input type="text" name="f_name" id="f_name" aria-label="First Name"
                                               placeholder="First Name" autocomplete="given-name" required>
                                        <span>First Name</span>
                                    </label>

                                    <label for="l_name" class="eb_label invert">
                                        <input type="text" name="l_name" id="l_name" aria-label="Last Name"
                                               placeholder="Last Name" autocomplete="family-name" required>
                                        <span>Last Name</span>
                                    </label>

                                    <label for="phone_no" class="eb_label invert expanded">
                                        <input type="tel" name="phone_no" id="phone_no" aria-label="Phone Number"
                                               placeholder="Phone Number" autocomplete="tel" required>
                                        <span>Phone Number</span>
                                    </label>

                                    <label for="email_add" class="eb_label invert expanded">
                                        <input type="email" name="email_add" id="email_add" aria-label="Email Address"
                                               placeholder="Email Address" autocomplete="email" required>
                                        <span>Email Address</span>
                                    </label>

                                    <label for="customer_course" class="eb_label invert expanded custom_select">
                                        <select name="customer_course" id="customer_course">
                                            <option value="" selected hidden>Which course are you interested in?
                                            </option>
                                            <option value="Lorem ipsum">Lorem ipsum</option>
                                            <option value="Dolor sit amet">Dolor sit amet</option>
                                            <option value="Consectetuer adipiscing elit">Consectetuer adipiscing elit
                                            </option>
                                            <option value="Aenean commodo">Aenean commodo</option>
                                            <option value="Ligula eget dolor">Ligula eget dolor</option>
                                        </select>
                                        <span>Which course are you interested in?</span>
                                    </label>

                                    <label for="gdpr_1" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_1">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I agree that my data will be used and stored as outlined in the <a
                                                    href="" target="_blank">Terms and Conditions</a> on the Easybook Training website
                                            </span>
                                    </label>

                                    <label for="gdpr_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_2">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I am happy to receive newsletters and promotional information from Easybook Training Ltd
                                            </span>
                                    </label>

                                    <div class="eb_contact_form_footer text-center">
                                        <button type="submit" class="button eb_btn" value="submit" tabindex="0"
                                                aria-label="Submit form">Submit
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 large-6"></div>
            </div>
        </div>
    </div>
</section>

<?php include('./template/footer.php'); ?>
