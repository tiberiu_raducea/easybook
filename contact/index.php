<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area up-down small just-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">We’d love to hear from you</h4>
                    <div class="eb_headline_sub">

                        <p>If you have any questions or queries about any of the <a href="">training courses available</a>, dates,
                            venues or bookings, get in touch below.</p>

                        <p>You can contact us by phone and speak to one of the customer services team on the number
                            below - we’re here seven days a week to take your call. Alternatively complete the simple
                            form and we’ll get back to you as soon as we can!</p>
                    </div>
                </div>

                <div class="grid-x grid-padding-x small-up-1 large-up-3">
                    <div class="cell">
                        <div class="contact_detail">
                            <div class="name"><span class="fa fa-map-marker"></span> Address:</div>
                            <div class="info">
                                Easybook Training, <br>3rd Floor, 11-13 Lonsdale Gardens, <br>Tunbridge Wells, <br>Kent, TN1 1NU.
                            </div>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="contact_detail">
                            <div class="name"><span class="fa fa-phone"></span> Phone:</div>
                            <div class="info">
                                08000 931 189
                            </div>
                        </div>
                    </div>
                    <div class="cell">
                        <div class="contact_detail">
                            <div class="name"><span class="fa fa-bullhorn"></span> Social:</div>
                            <div class="info">
                                <div class="eb_social colour">
                                    <a href="#" target="_blank" aria-label="Follow on Linkedin"><span aria-hidden="true" class="fa fa-linkedin"></span></a>
                                    <a href="#" target="_blank" aria-label="Like on Facebook"><span aria-hidden="true" class="fa fa-facebook"></span></a>
                                    <a href="#" target="_blank" aria-label="Follow on Twitter"><span aria-hidden="true" class="fa fa-twitter"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="eb_contact_area">
    <div class="eb_contact_background" style="background-image: url(/assets/img/contact_area.jpg)"></div>
    <div class="eb_contact_content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <form action="/" class="eb_contact_form" enctype="multipart/form-data" autocomplete="on">

                        <div class="grid-x grid-padding-x">
                            <div class="cell large-10">
                                <div class="eb_contact_header">
                                    <h2 class="title">Get in touch</h2>
                                    <p class="text">Simply leave your contact information below and we’ll call you back
                                        as
                                        soon as one of our team members becomes available.</p>
                                </div>

                                <div class="eb_contact_form_items">
                                    <label for="f_name" class="eb_label invert">
                                        <input type="text" name="f_name" id="f_name" aria-label="First Name"
                                               placeholder="First Name" autocomplete="given-name" required>
                                        <span>First Name</span>
                                    </label>

                                    <label for="l_name" class="eb_label invert">
                                        <input type="text" name="l_name" id="l_name" aria-label="Last Name"
                                               placeholder="Last Name" autocomplete="family-name" required>
                                        <span>Last Name</span>
                                    </label>

                                    <label for="email_add" class="eb_label invert expanded">
                                        <input type="email" name="email_add" id="email_add" aria-label="Email Address"
                                               placeholder="Email Address" autocomplete="email" required>
                                        <span>Email Address</span>
                                    </label>

                                    <label for="phone_no" class="eb_label invert expanded">
                                        <input type="tel" name="phone_no" id="phone_no" aria-label="Phone Number"
                                               placeholder="Phone Number" autocomplete="tel" required>
                                        <span>Phone Number</span>
                                    </label>

                                    <label for="client_message" class="eb_label invert expanded eb_textarea">
                                        <textarea name="message" id="client_message" cols="30" rows="5"></textarea>
                                        <span>Message</span>
                                    </label>

                                    <label for="gdpr_1" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_1">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I agree that my data will be used and stored as outlined in the <a
                                                    href="" target="_blank">Terms and Conditions</a> on the Easybook Training website
                                            </span>
                                    </label>

                                    <label for="gdpr_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_2">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I am happy to receive newsletters and promotional information from Easybook Training Ltd
                                            </span>
                                    </label>

                                    <div class="eb_contact_form_footer text-center">
                                        <button type="submit" class="button eb_btn" value="submit" tabindex="0"
                                                aria-label="Submit form">Submit
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 large-6"></div>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area up-down back_white up-down-out">
    <div class="feedback-wrap">
        <div class="feedback">
            <div class="feedback-slider">
                <?php for ($i = 0; $i < 10; $i++): ?>
                    <div class="feedback-slider-item">
                        <div class="feedback-element">
                            <div class="title">So simple, will 100% book again through EasyBook training</div>
                            <div class="quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                                ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                Nulla consequat
                                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                            </div>
                            <div class="author">Darren Rogan</div>
                            <div class="author-comp">Rogan Industries</div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>


<?php include('../template/footer.php'); ?>
