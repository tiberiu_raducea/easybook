<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Latest news and blog posts</h4>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut orav rationibus. In audiam tincidunt mel. Vim
                            ad adhuc augue, eos lorem velit decore in.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <form action="" class="post_filters">
                    <div class="grid-x grid-padding-x">
                        <div class="cell large-6">
                            <label for="postSearchInput" class="post_search_label">
                                <span class="fa fa-search"></span>
                                <input type="text" placeholder="Type here to search or select below"
                                       name="postSearchInput" id="postSearchInput">
                            </label>
                        </div>
                        <div class="cell large-6">
                            <label for="filterCategory" class="eb_select">
                                <select name="search_category" id="filterCategory">
                                    <option value="" selected hidden>Select a category</option>
                                    <option value="Category 1">Category 1</option>
                                    <option value="Category 2">Category 2</option>
                                    <option value="Category 3">Category 3</option>
                                    <option value="Category 4">Category 4</option>
                                    <option value="Category 5">Category 5</option>
                                </select>
                            </label>
                            <!--
                            <label for="filterByCategory" class="post_filter_select">
                                <select name="#" id="filterByCategory">
                                    <option value="0" selected disabled>Filter by category</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </label>
                            -->
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="grid-x grid-padding-x grid-padding-y align-center up-down small">
            <?php for ($i = 0; $i < 12; $i++): ?>
                <div class="cell large-5 medium-6 small-12">
                    <div class="post_article">
                        <a href="#" title="" class="post_article_thumb"
                           style="background-image: url(//picsum.photos/780/640?image=<?php echo 10 + $i ?>)"></a>
                        <div class="post_article_detail">
                            <a href="#" class="title" title="">Blog Title to go here</a>
                            <!-- Limit to maximum of 150 characters -->
                            <div class="excerpt">Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero
                                option. EtiamAd usu vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum.
                            </div>
                            <a href="#" class="link">read more</a>
                        </div>
                    </div>
                </div>
            <?php endfor; ?>
        </div>

        <div class="grdi-x grid-padding-x grid-padding-y">
            <div class="cell small-12 large-10 text-center">
                <nav aria-label="Pagination" class="pagination_block">
                    <ul class="pagination">
                        <!-- <li class="pagination-previous disabled">Previous <span class="show-for-sr">page</span></li> -->
                        <!-- <li class="pagination-previous"><a href="#" aria-label="Previous page">Previous <span class="show-for-sr">page</span></a></li> -->
                        <li class="current"><span class="show-for-sr">You're on page</span> 1</li>
                        <li><a href="#" aria-label="Page 2">2</a></li>
                        <li><a href="#" aria-label="Page 3">3</a></li>
                        <li><a href="#" aria-label="Page 4">4</a></li>
                        <li><a href="#" aria-label="Page 5">5</a></li>
                        <li><a href="#" aria-label="Page 6">6</a></li>
                        <li class="pagination-next"><a href="#" aria-label="Next page">Next <span class="show-for-sr">page</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>


</section>
<?php include('../template/footer.php'); ?>
