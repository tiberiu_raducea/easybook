<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="post_header">
                    <div class="grid-x grid-padding-x">
                        <div class="cell large-auto">
                            <div class="post_header_info">
                                <div class="post_category">
                                    <span>Category:</span> <a href="">Health and Safety</a>, <a href="">First Aid</a>
                                </div>
                                <div class="post_author">
                                    <span>Written by:</span> <a href="">John Smith</a> <span>30 October 2018</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell large-shrink">
                            <div class="post_header_share">
                                <span>share</span>
                                <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                            class="fa fa-linkedin"></span></a>
                                <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                            class="fa fa-facebook"></span></a>
                                <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                            class="fa fa-twitter"></span></a>
                                <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                            class="fa fa-whatsapp"></span></a>
                                <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                            class="fa fa-envelope"></span></a>
                                <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                            class="fa fa-link fa-rotate-90"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center">
                    <h1 class="eb_headline eb_headline_post">Blog Title Here</h1>
                    <div class="eb_headline_secondary">
                        <h5>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut oratio latine rationibus. In audiam tincidunt
                            mel.</h5>
                    </div>
                </div>
            </div>

            <div class="cell small-12 large-10">
                <!-- Child divs have clear in css so they don't render odd text after images-->
                <div class="post_article_content">
                    <div class="text-left">
                        <img src="//picsum.photos/470/260/" alt="" class="img-right">
                        <p>Mel eu diceret vocibus, has oratio delicata instructior eu, sit id impetus consulatu
                            omittantur.
                            Omnis euismod dignissim in mei, porro minimum nominati est ut. Eos autem tacimates electram
                            ea,
                            ne aperiri euismod sea. At omnis diceret quo, ea amet enim debitis vis, sea sanctus
                            explicari
                            suscipiantur et. Erat numquam dolorum te mel, ad sit dicta facilis explicari.</p>

                        <p>Ne amet alii munere mea. Nulla accommodare necessitatibus ei pri, deserunt periculis
                            evertitur ut
                            quo. Solet putent fuisset et vel, case democritum incorrupte eam in, mei no oblique iuvaret.
                            Debet impedit gubergren id nec. Appetere explicari adolescens ne mel, modo corpora commune
                            sed
                            in. Ad modus utinam usu, eum ad doctus impetus integre. Assum inani menandri te vim, his
                            detraxit erroribus et.</p>
                    </div>

                    <div class="text-right">
                        <img src="//picsum.photos/470/260/" alt="" class="img-left">
                        <p>Mel eu diceret vocibus, has oratio delicata instructior eu, sit id impetus consulatu
                            omittantur.
                            Omnis euismod dignissim in mei, porro minimum nominati est ut. Eos autem tacimates electram
                            ea,
                            ne aperiri euismod sea. At omnis diceret quo, ea amet enim debitis vis, sea sanctus
                            explicari
                            suscipiantur et. Erat numquam dolorum te mel, ad sit dicta facilis explicari.</p>

                        <p>Ne amet alii munere mea. Nulla accommodare necessitatibus ei pri, deserunt periculis
                            evertitur ut
                            quo. Solet putent fuisset et vel, case democritum incorrupte eam in, mei no oblique iuvaret.
                            Debet impedit gubergren id nec. Appetere explicari adolescens ne mel, modo corpora commune
                            sed
                            in. Ad modus utinam usu, eum ad doctus impetus integre. Assum inani menandri te vim, his
                            detraxit erroribus et.</p>
                    </div>

                    <div class="text-center">
                        <p>Mel eu diceret vocibus, has oratio delicata instructior eu, sit id impetus consulatu
                            omittantur. Omnis euismod dignissim in mei, porro minimum nominati est ut. Eos autem
                            tacimates electram ea, ne aperiri euismod sea. At omnis diceret quo, ea amet enim debitis
                            vis, sea sanctus explicari suscipiantur et. Erat numquam dolorum te mel, ad sit dicta
                            facilis explicari.</p>

                        <p>Ne amet alii munere mea. Nulla accommodare necessitatibus ei pri, deserunt periculis
                            evertitur ut quo. Solet putent fuisset et vel, case democritum incorrupte eam in, mei no
                            oblique iuvaret. Debet impedit gubergren id nec. Appetere explicari adolescens ne mel, modo
                            corpora commune sed in. Ad modus utinam usu, eum ad doctus impetus integre. Assum inani
                            menandri te vim, his detraxit erroribus et.</p>

                    </div>


                    <div class="text-center">
                        <div class="responsive-embed widescreen">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/nFZP8zQ5kzk"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center just-down">
                    <h5 class="eb_headline eb_headline_post">Featured Courses</h5>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut oratio latine rationibus. In audiam tincidunt
                            mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                    </div>
                </div>
            </div>

            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x grid-padding-y align-center small-up-1 medium-up-2 large-up-3"
                     data-equalizer data-equalize-by-row="true">
                    <?php for ($i = 0; $i < 3; $i++) : ?>
                        <div class="cell" data-equalizer-watch>
                            <div class="shortcut">
                                <div class="shortcut_image"
                                     style="background-image: url(//picsum.photos/480/320?image=<?php echo 40 + $i ?>)"></div>
                                <div class="shortcut_detail">
                                    <div class="title">Course Name <?php echo $i ?></div>
                                    <div class="excerpt">Information Here</div>
                                </div>

                                <a class="shortcut_link" href="#" aria-label="Course Name <?php echo $i ?>">Course
                                    Name <?php echo $i ?></a>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container up-down">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center just-down">
                    <h5 class="eb_headline eb_headline_post">Featured Team Members</h5>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut oratio latine rationibus. In audiam tincidunt
                            mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x grid-padding-y align-center small-up-1 medium-up-2">
                    <?php for ($i = 0; $i < 1; $i++) : ?>
                        <div class="cell">
                            <div class="eb_v_card">
                                <a href="#" class="eb_v_card_thumb"
                                   style="background-image: url(//picsum.photos/300/300);">
                                    <span>find out more</span>
                                </a>
                                <div class="eb_v_card_detail">
                                    <div class="info">
                                        <div class="title"><a href="#">Jane Smith</a></div>
                                        <div class="role">Job Title here</div>

                                        <div class="contact">
                                            <div class="contact_method">
                                                <span class="fa fa-phone"></span> Phone:
                                            </div>
                                            <a href="tel:08000 931 189">08000 931 189</a>
                                        </div>

                                        <div class="contact">
                                            <div class="contact_method">
                                                <span class="fa fa-envelope"></span> Email:
                                            </div>
                                            <a href="mailto:jane@easybooktraining.com">jane@easybooktraining.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container up-down just-down">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center">
                    <h5 class="eb_headline eb_headline_post">Featured Blog</h5>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut oratio latine rationibus. In audiam tincidunt
                            mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x grid-padding-y align-center">
                    <?php for ($i = 0; $i < 2; $i++): ?>
                        <div class="cell medium-6 small-12">
                            <div class="post_article">
                                <a href="#" title="" class="post_article_thumb"
                                   style="background-image: url(//picsum.photos/780/640?image=<?php echo 10 + $i ?>)"></a>
                                <div class="post_article_detail">
                                    <a href="#" class="title" title="">Blog Title to go here</a>
                                    <!-- Limit to maximum of 150 characters -->
                                    <div class="excerpt">Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero
                                        option. EtiamAd usu vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum.
                                    </div>
                                    <a href="#" class="link">read more</a>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('../template/footer.php'); ?>
