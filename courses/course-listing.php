<?php include('../template/header.php'); ?>


<?php include('../template/search.php'); ?>

    <section class="eb_content_area">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope=""
                            itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="eb_content_area">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell large-10 small-12">
                    <div class="eb_headline_block fluid text-center">
                        <h4 class="eb_headline">Book Online Today</h4>
                        <div class="eb_headline_sub">
                            <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu
                                vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes
                                ius, harum percipitur intellegebat sea eu, ius ut orav rationibus. In audiam tincidunt
                                mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="eb_content_area">
        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell small-12 large-10">
                    <form action="" class="post_filters">
                        <label for="inputSearch" class="post_search_label">
                            <span class="fa fa-search"></span>
                            <input type="text" placeholder="Type here to search or select below"
                                   name="postSearchInput" id="inputSearch" onkeyup="filterSearch()">
                        </label>
                    </form>
                </div>

                <div class="cell small-12 large-10">
                    <div class="column_listing">
                        <ul id="searchUnordered">
                            <li><a href="" title="Abrasive Wheels Course" aria-label="Abrasive Wheels Course">Abrasive
                                    Wheels Course</a></li>
                            <li><a href="" title="AED and CPR Course" aria-label="AED and CPR Course">AED and CPR
                                    Course</a></li>
                            <li><a href="" title="Asbestos Awareness Course" aria-label="Asbestos Awareness Course">Asbestos
                                    Awareness Course</a></li>
                            <li><a href="" title="CISRS Basic Scaffold Inspection Course"
                                   aria-label="CISRS Basic Scaffold Inspection Course">CISRS Basic Scaffold Inspection
                                    Course</a></li>
                            <li><a href="" title="CITB Health & Safety Awareness Course"
                                   aria-label="CITB Health & Safety Awareness Course">CITB Health & Safety Awareness
                                    Course</a></li>
                            <li><a href="" title="CITB Health & Safety for Directors Course"
                                   aria-label="CITB Health & Safety for Directors Course">CITB Health & Safety for
                                    Directors Course</a></li>
                            <li><a href="" title="CITB Site Environmental Awareness SEATS Course"
                                   aria-label="CITB Site Environmental Awareness SEATS Course">CITB Site Environmental
                                    Awareness SEATS Course</a></li>
                            <li><a href="" title="CITB SMSTS 5 Day Course" aria-label="CITB SMSTS 5 Day Course">CITB
                                    SMSTS 5 Day Course</a></li>
                            <li><a href="" title="CITB SMSTS Refresher 2 Day Course"
                                   aria-label="CITB SMSTS Refresher 2 Day Course">CITB SMSTS Refresher 2 Day Course</a>
                            </li>
                            <li><a href="" title="CITB SSSTS 2 Day Course" aria-label="CITB SSSTS 2 Day Course">CITB
                                    SSSTS 2 Day Course</a></li>
                            <li><a href="" title="CITB SSSTS Refresher 1 Day Course"
                                   aria-label="CITB SSSTS Refresher 1 Day Course">CITB SSSTS Refresher 1 Day Course</a>
                            </li>
                            <li><a href="" title="CITB Temporary Works Co-ordinator (TWCTC) Course"
                                   aria-label="CITB Temporary Works Co-ordinator (TWCTC) Course">CITB Temporary Works
                                    Co-ordinator (TWCTC) Course</a></li>
                            <li><a href="" title="CITB Temporary Works General Awareness (TWGATC) Course"
                                   aria-label="CITB Temporary Works General Awareness (TWGATC) Course">CITB Temporary
                                    Works General Awareness (TWGATC) Course</a></li>
                            <li><a href="" title="CITB Temporary Works Supervisor (TWSTC) Course"
                                   aria-label="CITB Temporary Works Supervisor (TWSTC) Course">CITB Temporary Works
                                    Supervisor (TWSTC) Course</a></li>
                            <li><a href="" title="City & Guilds 18th Edition 3 Day Course (2382-18)"
                                   aria-label="City & Guilds 18th Edition 3 Day Course (2382-18)">City & Guilds 18th
                                    Edition 3 Day Course (2382-18)</a></li>
                            <li><a href="" title="City & Guilds Confined Space Medium Risk 6150-02 Course"
                                   aria-label="City & Guilds Confined Space Medium Risk 6150-02 Course">City & Guilds
                                    Confined Space Medium Risk 6150-02 Course</a></li>
                            <li><a href="" title="ECITB - CCNSG Safety Passport Course"
                                   aria-label="ECITB - CCNSG Safety Passport Course">ECITB - CCNSG Safety Passport
                                    Course</a></li>
                            <li><a href="" title="ECITB - CCNSG Safety Passport Renewal Course"
                                   aria-label="ECITB - CCNSG Safety Passport Renewal Course">ECITB - CCNSG Safety
                                    Passport Renewal Course</a></li>
                            <li><a href="" title="ECITB CCNSG Leading a Team Safely Course"
                                   aria-label="ECITB CCNSG Leading a Team Safely Course">ECITB CCNSG Leading a Team
                                    Safely Course</a></li>
                            <li><a href="" title="IOSH Leading Safely" aria-label="IOSH Leading Safely">IOSH Leading
                                    Safely</a></li>
                            <li><a href="" title="IOSH Managing Safely Course" aria-label="IOSH Managing Safely Course">IOSH
                                    Managing Safely Course</a></li>
                            <li><a href="" title="IOSH Managing Safely Refresher Course"
                                   aria-label="IOSH Managing Safely Refresher Course">IOSH Managing Safely Refresher
                                    Course</a></li>
                            <li><a href="" title="IOSH Safety for Senior Executives Course"
                                   aria-label="IOSH Safety for Senior Executives Course">IOSH Safety for Senior
                                    Executives Course</a></li>
                            <li><a href="" title="IOSH Working Safely Course" aria-label="IOSH Working Safely Course">IOSH
                                    Working Safely Course</a></li>
                            <li><a href="" title="IPAF Operator Training Course - 3a & 3b"
                                   aria-label="IPAF Operator Training Course - 3a & 3b">IPAF Operator Training Course -
                                    3a & 3b</a></li>
                            <li><a href="" title="Ladder Association - Ladder and Step Ladder for Users Course"
                                   aria-label="Ladder Association - Ladder and Step Ladder for Users Course">Ladder
                                    Association - Ladder and Step Ladder for Users Course</a></li>
                            <li><a href="" title="Ladder Association - Ladder and Step Ladder Inspection Course"
                                   aria-label="Ladder Association - Ladder and Step Ladder Inspection Course">Ladder
                                    Association - Ladder and Step Ladder Inspection Course</a></li>
                            <li><a href="" title="Level 2 Award for Personal Licence Holders (APLH) Course"
                                   aria-label="Level 2 Award for Personal Licence Holders (APLH) Course">Level 2 Award
                                    for Personal Licence Holders (APLH) Course</a></li>
                            <li><a href="" title="Level 2 Award in Health and Safety in the Workplace Course"
                                   aria-label="Level 2 Award in Health and Safety in the Workplace Course">Level 2 Award
                                    in Health and Safety in the Workplace Course</a></li>
                            <li><a href="" title="Level 2 Award in the Principles of COSHH"
                                   aria-label="Level 2 Award in the Principles of COSHH">Level 2 Award in the Principles
                                    of COSHH</a></li>
                            <li><a href="" title="Level 3 Award in Health & Safety in the Workplace Course"
                                   aria-label="Level 3 Award in Health & Safety in the Workplace Course">Level 3 Award
                                    in Health & Safety in the Workplace Course</a></li>
                            <li><a href="" title="Level 4 Award in Health and Safety in the Workplace Course"
                                   aria-label="Level 4 Award in Health and Safety in the Workplace Course">Level 4 Award
                                    in Health and Safety in the Workplace Course</a></li>
                            <li><a href="" title="NEBOSH Certificate in Oil & Gas"
                                   aria-label="NEBOSH Certificate in Oil & Gas">NEBOSH Certificate in Oil & Gas</a></li>
                            <li><a href="" title="NEBOSH Construction Certificate"
                                   aria-label="NEBOSH Construction Certificate">NEBOSH Construction Certificate</a></li>
                            <li><a href="" title="NEBOSH Construction Certificate - Conversion"
                                   aria-label="NEBOSH Construction Certificate - Conversion">NEBOSH Construction
                                    Certificate - Conversion</a></li>
                            <li><a href="" title="NEBOSH Environmental Certificate"
                                   aria-label="NEBOSH Environmental Certificate">NEBOSH Environmental Certificate</a>
                            </li>
                            <li><a href="" title="NEBOSH Fire Certificate" aria-label="NEBOSH Fire Certificate">NEBOSH
                                    Fire Certificate</a></li>
                            <li><a href="" title="NEBOSH Fire Certificate - Conversion"
                                   aria-label="NEBOSH Fire Certificate - Conversion">NEBOSH Fire Certificate -
                                    Conversion</a></li>
                            <li><a href="" title="NEBOSH General Certificate" aria-label="NEBOSH General Certificate">NEBOSH
                                    General Certificate</a></li>
                            <li><a href="" title="NEBOSH International General Certificate"
                                   aria-label="NEBOSH International General Certificate">NEBOSH International General
                                    Certificate</a></li>
                            <li><a href="" title="NRSWA (New Roads & Street Works Act) Operatives Course"
                                   aria-label="NRSWA (New Roads & Street Works Act) Operatives Course">NRSWA (New Roads
                                    & Street Works Act) Operatives Course</a></li>
                            <li><a href="" title="NRSWA (New Roads & Street Works Act) Operatives Re-Assessment Course"
                                   aria-label="NRSWA (New Roads & Street Works Act) Operatives Re-Assessment Course">NRSWA
                                    (New Roads & Street Works Act) Operatives Re-Assessment Course</a></li>
                            <li><a href="" title="NRSWA (New Roads & Street Works Act) Supervisor Course"
                                   aria-label="NRSWA (New Roads & Street Works Act) Supervisor Course">NRSWA (New Roads
                                    & Street Works Act) Supervisor Course</a></li>
                            <li><a href="" title="NRSWA (New Roads & Street Works Act) Supervisor Re-Assessment Course"
                                   aria-label="NRSWA (New Roads & Street Works Act) Supervisor Re-Assessment Course">NRSWA
                                    (New Roads & Street Works Act) Supervisor Re-Assessment Course</a></li>
                            <li><a href="" title="NRSWA Unit 1 Signing, Lighting and Guarding"
                                   aria-label="NRSWA Unit 1 Signing, Lighting and Guarding">NRSWA Unit 1 Signing,
                                    Lighting and Guarding</a></li>
                            <li><a href="" title="NRSWA Unit S1 Signing, Lighting and Guarding"
                                   aria-label="NRSWA Unit S1 Signing, Lighting and Guarding">NRSWA Unit S1 Signing,
                                    Lighting and Guarding</a></li>
                            <li><a href="" title="PASMA Training (Standard Tower Course)"
                                   aria-label="PASMA Training (Standard Tower Course)">PASMA Training (Standard Tower
                                    Course)</a></li>
                            <li><a href="" title="Scottish Certificate for Personal Licence Holders (SCPLH) Course"
                                   aria-label="Scottish Certificate for Personal Licence Holders (SCPLH) Course">Scottish
                                    Certificate for Personal Licence Holders (SCPLH) Course</a></li>

                        </ul>
                    </div>
                </div>
            </div>


        </div>

        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-12 large-10">
                    <div class="eb_cms_content formatted text-center up-down just-down">
                        <p>Easybook Training offer a variety of Accredited Health & Safety training courses available to
                            book online, including
                            <a href="">SMSTS Courses</a>, <a href="">IOSH Training</a>, <a href="">SSSTS Courses</a>, <a
                                    href="">CITB Temporary Works Co-ordinator Training (TWCTC)</a> and <a href="">NEBOSH
                                courses</a>. We work with hundreds of accredited UK Training Providers to deliver you a
                            choice from 1000’s of Health and Safety Training Courses at UK venues local to you and at
                            great prices.</p>

                        <p>View all our available Health and Safety training dates and locations to find the most
                            convenient course for you today. If you can’t find what you are looking for, please give us
                            a call and one of our friendly customer service team will be happy to help.</p>

                    </div>
                </div>
            </div>
        </div>

    </section>

<?php include('../template/footer.php'); ?>