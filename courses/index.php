<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

    <section class="eb_content_area">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope=""
                            itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="eb_content_area">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell large-10 small-12">
                    <div class="eb_headline_block fluid text-center">
                        <h2 class="eb_headline">Lorem ipsum dolor sit amet (h2)</h2>
                        <div class="eb_headline_sub">
                            <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu
                                vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes
                                ius, harum percipitur intellegebat sea eu, ius ut orav rationibus. In audiam tincidunt
                                mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-container fluid">
            <div class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-4 course_box_listing">

                <?php $courses = [
                    [
                        'title' => 'Health & Safety',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/health_and_safety.jpg',
                        'sale' => false
                    ],
                    [
                        'title' => 'Construction',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/construction.jpg',
                        'sale' => false
                    ],
                    [
                        'title' => 'First Aid',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/first_aid.jpg',
                        'sale' => false
                    ],
                    [
                        'title' => 'Fire Safety',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/fire_safety.jpg',
                        'sale' => false
                    ],
                    [
                        'title' => 'Plant',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/plant.jpg',
                        'sale' => false
                    ],
                    [
                        'title' => 'Working at Height',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/working_at_height.jpg',
                        'sale' => false
                    ],
                    [
                        'title' => 'Sale',
                        'text' => 'Training Courses',
                        'image_url' => 'assets/img/courses/course_sale.jpg',
                        'sale' => true
                    ],
                ] ?>
                <?php foreach ($courses as $course) : ?>
                    <div class="cell">
                        <div class="course_box <?php if ($course['sale']) : ?>on_sale<?php endif; ?>">
                            <div class="course_box_image"
                                 style="background-image: url(<?php echo $site_url . $course['image_url'] ?>)"></div>
                            <div class="course_box_content" aria-hidden="true">
                                <div class="course_box_title"><?php echo $course['title'] ?></div>
                                <div class="course_box_text"><?php echo $course['text'] ?></div>
                            </div>
                            <div class="course_box_overlay">
                                <div class="overlay_head">
                                    <span><?php echo $course['title'] ?></span>
                                    <img src="../assets/img/eg_health_safety_icon.png" alt="">
                                </div>
                                <div class="overlay_body">
                                    <dl>
                                        <dt>NEBOSH Training Courses</dt>
                                        <dd>
                                            <ul>
                                                <li>NEBOSH General Certificate</li>
                                                <li>NEBOSH Environmental Certificate</li>
                                            </ul>
                                        </dd>
                                        <dt>IOSH Training Courses</dt>
                                        <dd>
                                            <ul>
                                                <li>IOSH Managing Safely</li>
                                                <li>IOSH Directing Safely</li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="overlay_footer">
                                    <button class="button eb_btn">view courses</button>
                                </div>
                            </div>
                            <a href="#" class="course_box_link" aria-label="<?php echo $course['title'] ?>"
                               title="<?php echo $course['title'] ?>"><?php echo $course['title'] ?></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-12 large-10">
                    <div class="eb_cms_content formatted text-center up-down just-down">
                        <p>Working with hundreds of the country’s most respected <a href="â">accredited training providers</a> and
                            training centres, Easybook Training is sure to be able to find the training course to suit
                            your requirements.</p>

                        <p>Located all over the UK, our nationwide network of <a href="#">training providers</a> run <a
                                    href="#">training courses</a>
                            covering a huge range of sectors including health and safety courses, construction courses,
                            fire safety training, first aid courses, food safety/hygiene courses and working at height
                            courses. Easybook Training’s simple online search tool helps you narrow down from a list of
                            1000s of training courses to find the perfect training course that meets all your
                            requirements regarding location, price and date.</p>
                        <p>Once you’ve found what you’re looking for
                            you can book and pay for your <a href="#">training course online</a> simply and securely via our
                            website.</p>

                        <p>The Easybook service doesn’t stop at booking a training course with you. Once you have found
                            your perfect course you receive a full after care package, which includes the Easybook
                            Reminder. This handy service sends candidates/employers a notice of any certificates that
                            need renewing, so once your training is complete you can be sure that your paperwork stays
                            as up to date as your knowledge!</p>

                        <p>A member of the Easybook team is available on the phone seven days a week, should you want
                            any advice or assistance at any stage of the process. As a family-run business we’re keen to
                            make sure that we always provide the personal touch – after all, everyone is an individual
                            with individual requirements so we like to make sure you’re treated as such!</p>

                    </div>
                </div>
            </div>
        </div>

    </section>

<?php include('../template/footer.php'); ?>