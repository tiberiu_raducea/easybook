<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

    <section class="eb_content_area up-down just-down">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell large-10 small-12">
                    <div class="eb_headline_block fluid text-center">
                        <h4 class="eb_headline">Select NVQ Course Level</h4>
                        <div class="eb_headline_sub strong">

                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
                                voluptate velit esse cillum dolore eu fugiat nulla pariatur,</p>
                        </div>
                    </div>
                </div>
                <div class="cell large-10 small-12">
                    <form action="" class="post_filters">
                        <label for="inputSearch" class="post_search_label">
                            <span class="fa fa-search"></span>
                            <input type="text" placeholder="Type here to search or select below"
                                   name="postSearchInput" id="inputSearch" onkeyup="filterSearch()">
                        </label>
                    </form>
                </div>
            </div>
        </div>


        <div class="grid-container fluid  up-down small just-up">
            <ul class="grid-x grid-padding-x grid-padding-y small-up-1 medium-up-2 large-up-4"
                 data-equalizer data-equalize-by-row="true" id="searchUnordered">
                <?php for ($i = 1; $i < 25; $i++) : ?>
                    <li class="cell" data-equalizer-watch>
                        <div class="shortcut hover">
                            <div class="shortcut_image"
                                 style="background-image: url(//picsum.photos/680/320?image=<?php echo 33 + $i ?>)"></div>
                            <div class="shortcut_detail">
                                <div class="title">NVQ course name <?php echo $i ?></div>
                            </div>
                            <div class="shortcut_overlay">
                                <button role="button" class="button eb_btn shrunk">View details & enrol</button>
                            </div>
                            <a class="shortcut_link" href="#" aria-label="NVQ course name <?php echo $i ?>">NVQ course name <?php echo $i ?></a>
                        </div>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>

    </section>




<?php include('../template/footer.php'); ?>