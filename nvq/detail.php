<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="grid-x grid-padding-x">
                    <div class="cell large-auto">
                        <div class="breadcrumbs-container">
                            <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                        <span itemprop="name">Home</span>
                                    </a>
                                    <meta itemprop="position" content="1">
                                </li>
                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                       title="Breadcrumb link 1">
                                        <span itemprop="name">Breadcrumb link 1</span>
                                    </a>
                                    <meta itemprop="position" content="2">
                                </li>

                                <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                    <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                       title="Breadcrumb link 2">
                                        <span itemprop="name">Breadcrumb link 2</span>
                                    </a>
                                    <meta itemprop="position" content="3">
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="cell large-shrink">
                        <div class="post_header_share">
                            <span>share</span>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-linkedin"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-facebook"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-twitter"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-whatsapp"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-envelope"></span></a>
                            <a href="#" aria-label="Share on ..." title="Share this on ..."><span
                                        class="fa fa-link fa-rotate-90"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="eb_content_area up-down just-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">

            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center">
                    <h1 class="eb_headline eb_headline_post">NVQ Course 7</h1>
                </div>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x">
                    <div class="cell small-12 large-4 order-1 large-order-2">
                        <div class="sidebar_block">
                            <div class="book_form">
                                <div class="book_form_head text-center">
                                    Book Now <img src="/assets/img/eb_uk.png" alt="Nationwide coverage">
                                </div>
                                <div class="book_form_body">
                                    <div class="info">Price per person <span class="site_currency">&pound;</span><span id="bookinValue">250</span> <sup>ex VAT</sup></div>
                                    <form action="/" method="" enctype="multipart/form-data">
                                        <div class="inline_input">
                                            <label for="numberOfDelegates">No. of delegates</label>
                                            <div class="custom_select shade">
                                                <select name="" id="numberOfDelegates" onchange="updateTotal()">
                                                    <option value="1">1</option>
                                                    <option value="2" selected>2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <!--                                                    <option value="10">10</option>
                                                                                                        <option value="11">11</option>
                                                                                                        <option value="12">12</option>-->
                                                </select>
                                            </div>

                                        </div>
                                        <div class="book_form_total">
                                            <div class="name">total</div>
                                            <div class="value"><span class="site_currency">&pound;</span><span id="pricePerBooking">500</span><sup>ex VAT</sup></div>
                                        </div>

                                        <div class="book_form_footer">
                                            <button type="submit" class="button eb_btn shrunk">Enrol</button>
                                        </div>
                                    </form>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="cell small-12 large-8 order-2 large-order-1">
                        <div class="eb_cms_content formatted">
                            <h4>Overview</h4>
                            <p>This two day SSSTS training course is intended for those who have, or are about to acquire,
                                supervisory responsibilities. Through completing the course supervisors will be provided with an
                                ability to understand health, safety, welfare and environmental issues, as well as their legal
                                responsibilities relevant to their work activities. It will highlight the requirement to promote
                                health and safety to supervise effectively.</p>

                            <p>The SSSTS course is endorsed by the United Kingdom Contractors Group as the standard training for
                                all supervisors working on UKCG sites.</p>

                            <p>Delegates under the age of 18 must provide written consent from a parent or guardian to
                                successfully attend.</p>

                            <p><strong>* PLEASE NOTE - It will take approximately 6-8 weeks for the CITB to issue certification
                                    *</strong></p>

                            <h4>Objectives</h4>
                            <p>On successful completion of the SSSTS training course delegates that are given supervisory
                                responsibilities will understand:</p>


                            <ul>
                                <li>Why they are carrying out their identified duties</li>
                                <li>What is expected of them</li>
                                <li>Ensure they contribute to the safety of the workpltace</li>
                            </ul>

                            <h4>Agenda</h4>
                            <p>The SSSTS training course willt cover the following:</p>
                            <ul>
                                <li>Health & Safety at Work Act</li>
                                <li>The Role of the Supervisor</li>
                                <li>Behavioural Safety in the Workplace</li>
                                <li>PPE</li>
                                <li>Site Inductions & Toolbox Talks</li>
                                <li>Risk assessments and the need for method statements</li>
                                <li>Monitor site activities effectively</li>
                            </ul>

                            <h4>Duration</h4>
                            <p>2 Days</p>

                            <h4>Assessment</h4>
                            <p>At the end of the CITB SSSTS course delegates are required to sit an exam paper consisting of 25
                                questions, of which 4 are safety critical and require a short written answer. Multiple choice
                                questions are worth 1 point each and safety critical questions are worth 1-3 points each. In
                                order to pass the course delegates MUST answer ALL of safety critical questions correctly. The
                                SSSTS Course pass rate is 80% (24 out of 30)</p>

                            <p>Delegates who successfully complete the projects and assessment are awarded the CITB SSSTS - Site
                                Supervision Safety Training Scheme certificate which is valid for 5 years.</p>

                            <h4>Who Should Attend?</h4>
                            <p>This course is aimed at first line managers, foreman, gangers, team leaders, supervisors and
                                those who have, or are about to acquire supervisory responsibilities.</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</section>


<section class="eb_content_area up-down back_white">
    <div class="feedback-wrap">
        <div class="feedback">
            <div class="feedback-slider">
                <?php for ($i = 0; $i < 10; $i++): ?>
                    <div class="feedback-slider-item">
                        <div class="feedback-element">
                            <div class="title">So simple, will 100% book again through EasyBook training</div>
                            <div class="quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                                ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                Nulla consequat
                                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                            </div>
                            <div class="author">Darren Rogan</div>
                            <div class="author-comp">Rogan Industries</div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Similar NVQS</h4>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container fluid">
        <div class="grid-x grid-padding-x align-center small-up-1 medium-up-2 large-up-4">
            <div class="cell">
                <div class="shortcut hover">
                    <div class="shortcut_image"
                         style="background-image: url(//picsum.photos/680/320?image=33)"></div>
                    <div class="shortcut_detail">
                        <div class="title resized">Emergency First Aid <br>at Work Course</div>
                    </div>
                    <div class="shortcut_overlay">
                        <button role="button" class="button eb_btn shrunk">View details & enrol</button>
                    </div>
                    <a class="shortcut_link" href="#" aria-label="Emergency First Aid at Work Course">Emergency First Aid <br>at Work Course</a>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="eb_content_area">
    <div class="grid-container up-down">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="eb_headline_block fluid text-center just-down">
                    <h5 class="eb_headline eb_headline_post">Featured Team Members</h5>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu vero
                            option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes ius,
                            harum percipitur intellegebat sea eu, ius ut oratio latine rationibus. In audiam tincidunt
                            mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x grid-padding-y align-center small-up-1 medium-up-2">
                    <?php for ($i = 0; $i < 1; $i++) : ?>
                        <div class="cell">
                            <div class="eb_v_card">
                                <a href="#" class="eb_v_card_thumb"
                                   style="background-image: url(//picsum.photos/300/300);">
                                    <span>find out more</span>
                                </a>
                                <div class="eb_v_card_detail">
                                    <div class="info">
                                        <div class="title"><a href="#">Jane Smith</a></div>
                                        <div class="role">Job Title here</div>

                                        <div class="contact">
                                            <div class="contact_method">
                                                <span class="fa fa-phone"></span> Phone:
                                            </div>
                                            <a href="tel:08000 931 189">08000 931 189</a>
                                        </div>

                                        <div class="contact">
                                            <div class="contact_method">
                                                <span class="fa fa-envelope"></span> Email:
                                            </div>
                                            <a href="mailto:jane@easybooktraining.com">jane@easybooktraining.com</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('../template/footer.php'); ?>
