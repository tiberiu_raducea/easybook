<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">E-Learning - Online Health and Safety Courses</h4>
                    <div class="eb_headline_sub">
                        <p>E-learning courses can be the solution for anyone who is looking for more flexibility from
                            their learning programme. <br>
                            Easybook Training work closely with a number of approved distance/e-learning providers to
                            offer a range of flexible <a href="">online health and safety courses</a> and e-learning
                            courses for those
                            who don’t wish to be tied to traditional <a href="">classroom-based training course.</a></p>
                    </div>
                </div>
            </div>


            <div class="cell large-10 small-12">
                <div class="online_course_listing up-down-out only-down">
                    <?php for ($i = 0; $i < 10; $i++): ?>
                        <div class="e_course_row">
                            <div class="online_course_item">
                                <div class="icon">
                                    <?php if ($i == 4) : ?><span class="ribbon_label new">New!</span> <?php endif; ?>
                                    <img src="/assets/img/icons/e_icon_1.png" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">Bribery Awareness Act</div>
                                </div>
                                <a href="#" class="link">Bribery Awareness Act</a>
                            </div>

                            <div class="online_course_item">
                                <div class="icon">
                                    <?php if ($i == 7) : ?><span class="ribbon_label sale">Sale!</span> <?php endif; ?>
                                    <img src="/assets/img/icons/e_icon_2.png" alt="">
                                </div>
                                <div class="info">
                                    <div class="title">Fuel Safety</div>
                                </div>
                                <a href="#" class="link">Fuel Safety</a>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>

            <div class="cell large-10 small-12">
                <div class="eb_cms_content formatted text-center up-down just-down">
                    <p>Contrary to popular belief, with an e-learning/online training course you are not left to
                        struggle alone with complicated materials. Today’s online training courses are fully
                        interactive, with students being able to take advantage of a wide variety of support tools to
                        enable them to get the very best out of their chosen training course.</p>

                    <p>All Easybook e-learning courses are held with <a href="">accredited UK training providers</a>,
                        and have been
                        developed to ensure that students receive the help and support of fully qualified tutors for the
                        duration of the <a href="">online course</a>. Using non-traditional communication methods such
                        as email, Skype
                        or Facebook, students are encouraged to interact with tutors and other e-learners in the same
                        way as you would in a traditional classroom situation. Many online training providers also use
                        YouTube to post videos to assist with study and revision.</p>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="eb_contact_area">
    <div class="eb_contact_background" style="background-image: url(/assets/img/eb_contact_background.jpg)"></div>
    <div class="eb_contact_content">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-12 large-6">
                    <form action="/" class="eb_contact_form" enctype="multipart/form-data" autocomplete="on">

                        <div class="grid-x grid-padding-x">
                            <div class="cell large-10">
                                <div class="eb_contact_header">
                                    <h2 class="title">Book your E-learning Course</h2>
                                    <p class="text">Need to book your E-Learning or can't find what you're looking for? Please
                                        get in touch with out booking team by calling us free on <strong style="white-space: nowrap">0800 931 189</strong>
                                        or complete the form below:</p>
                                </div>
                                <div class="eb_contact_form_items">
                                    <label for="f_name" class="eb_label invert">
                                        <input type="text" name="f_name" id="f_name" aria-label="First Name"
                                               placeholder="First Name" autocomplete="given-name" required>
                                        <span>First Name</span>
                                    </label>

                                    <label for="l_name" class="eb_label invert">
                                        <input type="text" name="l_name" id="l_name" aria-label="Last Name"
                                               placeholder="Last Name" autocomplete="family-name" required>
                                        <span>Last Name</span>
                                    </label>

                                    <label for="phone_no" class="eb_label invert expanded">
                                        <input type="tel" name="phone_no" id="phone_no" aria-label="Phone Number"
                                               placeholder="Phone Number" autocomplete="tel" required>
                                        <span>Phone Number</span>
                                    </label>

                                    <label for="email_add" class="eb_label invert expanded">
                                        <input type="email" name="email_add" id="email_add" aria-label="Email Address"
                                               placeholder="Email Address" autocomplete="email" required>
                                        <span>Email Address</span>
                                    </label>

                                    <label for="customer_course" class="eb_label invert expanded custom_select">
                                        <select name="customer_course" id="customer_course">
                                            <option value="" selected hidden>Which course are you interested in?
                                            </option>
                                            <option value="Lorem ipsum">Lorem ipsum</option>
                                            <option value="Dolor sit amet">Dolor sit amet</option>
                                            <option value="Consectetuer adipiscing elit">Consectetuer adipiscing elit
                                            </option>
                                            <option value="Aenean commodo">Aenean commodo</option>
                                            <option value="Ligula eget dolor">Ligula eget dolor</option>
                                        </select>
                                        <span>Which course are you interested in?</span>
                                    </label>

                                    <label for="gdpr_1" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_1">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I agree that my data will be used and stored as outlined in the <a
                                                    href="" target="_blank">Terms and Conditions</a> on the Easybook Training website
                                            </span>
                                    </label>

                                    <label for="gdpr_2" class="eb_gdpr_label invert">
                                        <input type="checkbox" name="" id="gdpr_2">
                                        <span class="gdpr_text">
                                                <span class="gdpr_tick"></span>
                                                I am happy to receive newsletters and promotional information from Easybook Training Ltd
                                            </span>
                                    </label>

                                    <div class="eb_contact_form_footer text-center">
                                        <button type="submit" class="button eb_btn" value="submit" tabindex="0"
                                                aria-label="Submit form">Submit
                                        </button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>
                <div class="cell small-12 large-6"></div>
            </div>
        </div>
    </div>
</section>


<section class="eb_content_area up-down up-down-out only-down back_white">
    <div class="feedback-wrap">
        <div class="feedback">
            <div class="feedback-slider">
                <?php for ($i = 0; $i < 10; $i++): ?>
                    <div class="feedback-slider-item">
                        <div class="feedback-element">
                            <div class="title">So simple, will 100% book again through EasyBook training</div>
                            <div class="quote">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
                                ligula eget
                                dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                nascetur
                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
                                Nulla consequat
                                massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
                            </div>
                            <div class="author">Darren Rogan</div>
                            <div class="author-comp">Rogan Industries</div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>
<?php include('../template/footer.php'); ?>
