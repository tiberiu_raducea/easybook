<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Training Venues in the South East</h4>
                    <div class="eb_headline_sub">

                        <p>Our network of approved training providers offer a wide variety of first aid courses,
                            construction courses, fire safety training and health and safety training courses in the
                            South East. Please click on any of the locations listed below to find out more about the
                            individual training venues in the South East and the training courses and dates currently
                            available at these training venues.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="eb_content_area up-down just-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <dl class="venue_listing">
                    <?php for ($i = 0; $i < rand(3, 10); $i++) : ?>
                        <dt><a href="#" aria-label="Berkshire" title="Berkshire">Berkshire</a></dt>
                        <dd class="">
                            <dl class="venue_listing_dropdown content_in_columns col_3">
                                <?php for ($x = 0; $x < rand(3, 9); $x++) : ?>
                                    <dt><a href="#" aria-label="Aeplace, Berkshire" title="Aeplace, Berkshire">Aeplace,
                                            Berkshire</a></dt>
                                    <dd>
                                        <ul>
                                            <li><a href="#" title="Lorem Ipsum Training, Solor Doit, PO1 5CO"
                                                   aria-label="Lorem Ipsum Training, Solor Doit, PO1 5CO">Lorem Ipsum
                                                    Training, Solor Doit, PO1 5CO</a></li>
                                            <li><a href="#" title="Lorem Ipsum Training, Solor Doit, PO1 5CO"
                                                   aria-label="Lorem Ipsum Training, Solor Doit, PO1 5CO">Lorem Ipsum
                                                    Training, Solor Doit, PO1 5CO</a></li>
                                            <li><a href="#" title="Lorem Ipsum Training, Solor Doit, PO1 5CO"
                                                   aria-label="Lorem Ipsum Training, Solor Doit, PO1 5CO">Lorem Ipsum
                                                    Training, Solor Doit, PO1 5CO</a></li>
                                        </ul>
                                    </dd>
                                <?php endfor; ?>
                            </dl>
                        </dd>
                    <?php endfor; ?>

                </dl>
            </div>
        </div>
    </div>
</section>
<?php include('../template/footer.php'); ?>
