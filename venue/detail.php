<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

<section class="eb_content_area">

    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell large-10 medium-11 small-12">
                <div class="breadcrumbs-container">
                    <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                <span itemprop="name">Home</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 1">
                                <span itemprop="name">Breadcrumb link 1</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                               title="Breadcrumb link 2">
                                <span itemprop="name">Breadcrumb link 2</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell large-10 small-12">
                <div class="eb_headline_block fluid text-center">
                    <h4 class="eb_headline">Venue Details
                    </h4>
                    <div class="eb_headline_sub">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat.</p>

                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                            mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                            veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>

                        <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                            consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam
                            est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non
                            numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                            voluptatem.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x">
                    <div class="cell large-4 small-12">
                        <div class="up-down small">
                            <h4>Venue Details</h4>
                            <div class="contact_detail">
                                <div class="name"><span class="fa fa-map-marker"></span> Address:</div>
                                <div class="info">
                                    Crowthorne Road North , Bracknell, Berkshire,
                                    RG12 7AU , UK
                                </div>
                            </div>

                            <div class="contact_detail">
                                <div class="name"><span class="fa fa-phone"></span> Phone:</div>
                                <div class="info">
                                    08000 931 189
                                </div>
                            </div>

                            <a class="get_directions" target="_blank" href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2488.557956025403!2d-0.7537711842335653!3d51.41117647961947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48767e1e411fdbe7%3A0xc601ba2ce6ee9b51!2sCrowthorne+Rd+N%2C+Bracknell+RG12+7AU%2C+UK!5e0!3m2!1sen!2sro!4v1545142457802">Get directions</a>
                        </div>

                    </div>
                    <div class="cell large-8 small-12">
                        <div class="responsive-embed widescreen">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2488.557956025403!2d-0.7537711842335653!3d51.41117647961947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48767e1e411fdbe7%3A0xc601ba2ce6ee9b51!2sCrowthorne+Rd+N%2C+Bracknell+RG12+7AU%2C+UK!5e0!3m2!1sen!2sro!4v1545142457802" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="eb_content_area up-down small just-up">
    <div class="grid-container">
        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="cell small-12 large-10">
                <h5 class="text-center">Upcoming Courses at this venue</h5>
            </div>
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x align-center align-middle">
                    <div class="cell large-shrink">
                        <div class="table_info"><span class="fa fa-envelope"></span>Save & Send <i>?</i></div>
                        <div class="table_info"><span class="fa fa-g">G</span> Identifies courses which are eligible for the CITB Grant</div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 large-12 xlarge-10">
                <?php for ($x = 0; $x < 1; $x++): ?>
                    <div class="eb_search_table">
                        <div class="eb_search_table_header">
                            <div class="eb_search_table_row">
                                <div class="eb_search_table_data"><span class="name">18 Stree Name, County (City) <span>xx.x miles away</span></span></div>
                                <div class="eb_search_table_data large-text-right"><a href="#" class="table_head_link">View on map</a></div>
                            </div>

                        </div>
                        <div class="eb_search_table_body">

                            <div class="eb_search_table_row defined">
                                <div class="eb_search_table_data"><span class="table_sub_title">Course</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Start Date</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">End Date</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Delivery</span></div>
                                <div class="eb_search_table_data"><span class="table_sub_title">Price <sup>exVAT</sup></span></div>
                                <div class="eb_search_table_data"></div>
                                <div class="eb_search_table_data"></div>
                            </div>

                            <?php for ($i = 0; $i < 7; $i++) : ?>
                                <div class="eb_search_table_row defined <?php if ($i == 3): ?>item_on_sale<?php endif; ?>">
                                    <div class="eb_search_table_data">
                                        <a href="#" title="CITB SSSTS <?php echo $i ?> Day Course" class="name">CITB SSSTS <?php echo $i ?> Day Course <?php if ($i == 3): ?>
                                                <sup>sale</sup><?php endif; ?></a></div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">0<?php echo $i + 1 ?> Jan 10</span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">31 Dec 10</span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <span class="plain_value">Block</span></div>
                                    <div class="eb_search_table_data">
                                        <span class="item_price <?php if ($i == 3): ?>reduced<?php endif; ?>"><span
                                                    class="site_currency">&pound;</span>250 <?php if ($i == 3): ?><sup>&pound;350</sup><?php endif; ?> </span>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <div class="item_cta equalized">


                                            <?php if ($i == 2) : ?><a href="" class="button eb_btn shrunk light"><span class="breakpoint">please</span>
                                                call</a><?php elseif ($i == 5) : ?><a href=""
                                                                                      class="button eb_btn shrunk dark">call
                                                <span class="breakpoint">to book</span></a><?php else: ?><a href="" class="button eb_btn shrunk">view
                                                <span class="breakpoint">details</span></a><?php endif; ?>
                                            <a href="#"><span class="fa fa-envelope"></span></a>


                                        </div>
                                    </div>
                                    <div class="eb_search_table_data">
                                        <div class="item_cta"><?php if (!($i % 3)) : ?><span
                                                    class="fa fa-g">G</span><?php endif; ?> <?php if (!($i % 6)) : ?><a
                                                    href=""
                                                    class="button eb_btn shrunk">Book</a><?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                        <div class="eb_search_table_footer">
                            <div class="eb_search_table_row">
                                <div class="eb_search_table_data">
                                    <a href=""><span class="fa fa-plus"></span> view more courses</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</section>



<?php include('../template/footer.php'); ?>
