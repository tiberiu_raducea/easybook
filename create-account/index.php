<?php include('../template/header-checkout.php'); ?>

<section class="eb_login_area is_hidden <?php if ($status) : ?>logged_in<?php endif; ?>" data-toggler="is_hidden" id="logInArea">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-10">
                <div class="grid-x grid-padding-x">
                    <div class="cell small-12 large-9">
                        <form action="" class="eb_login_form">
                            <div class="grid-x grid-padding-x  align-middle">
                                <div class="cell large-shrink">
                                    <label for="logUserEmail" class="eb_login_form_headline">
                                        <span class="fa fa-user"></span>
                                        Log in
                                    </label>
                                </div>
                                <div class="cell large-auto">
                                    <label class="eb_label invert">
                                        <input type="email" id="logUserEmail" aria-label="Email Address"
                                               autocomplete="user-name"
                                               placeholder="Email Address" required="">
                                        <span>Email Address</span>
                                    </label>
                                </div>
                                <div class="cell large-auto">
                                    <label for="" class="eb_label invert">
                                        <input type="password" placeholder="Password (case sensitive)"
                                               aria-label="case sensitive password" autocomplete="current-password"
                                               required=""
                                               aria-describedby="passwordHelpText">
                                        <span>Password (case sensitive)</span>
                                    </label>
                                    <p class="help-text" id="passwordHelpText"><a href="#">I've forgotten my
                                            password</a></p>
                                </div>
                                <div class="cell large-shrink">
                                    <button type="submit" class="button eb_btn" aria-label="Log in">Log in</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="cell small-12 large-3">
                        <a href="#" class="eb_create_account">
                            <span>Don't have an EasyBook login?</span>
                            <span>Create an account</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<section class="eb_content_area up-down">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-12 large-5">
                <form action="" class="register_account">
                    <div class="stage_details open">
                        <div class="stage_details_body">
                            <div class="eb_checkout_form_items">
                                <label for="register_title"
                                       class="eb_label expanded eb_select">
                                    <select name="register_title"
                                            id="register_title"
                                            required>
                                        <option value="" selected="selected"
                                                hidden>Title
                                        </option>
                                        <option value="mr">Mr</option>
                                        <option value="mrs">Mrs</option>
                                        <option value="miss">Miss</option>
                                        <option value="ms">Ms</option>
                                        <option value="mx">Mx</option>
                                        <option value="sir">Sir</option>
                                        <option value="dr">Dr</option>
                                        <option value="lady">Lady</option>
                                        <option value="lord">Lord</option>
                                    </select>
                                </label>

                                <label for="register_f_name"
                                       class="eb_label">
                                    <input type="text"
                                           name="register_f_name"
                                           id="register_f_name"
                                           aria-label="First Name"
                                           placeholder="First Name*"
                                           autocomplete="given-name"
                                           required>
                                    <span>First Name*</span>
                                </label>

                                <label for="register_l_name"
                                       class="eb_label">
                                    <input type="text"
                                           name="register_l_name"
                                           id="register_l_name"
                                           aria-label="Last Name"
                                           placeholder="Last Name*"
                                           autocomplete="family-name"
                                           required>
                                    <span>Last Name*</span>
                                </label>

                                <label for="register_job_title" class="eb_label expanded">
                                    <input type="text" name="checkout_job_title"
                                           id="checkout_job_title" aria-label="Job Title"
                                           placeholder="Job Title"
                                           autocomplete="organization-title">
                                    <span>Job Title*</span>
                                </label>

                                <label for="register_email_add"
                                       class="eb_label expanded">
                                    <input type="email"
                                           name="register_email_add"
                                           id="register_email_add"
                                           aria-label="Email Address"
                                           placeholder="Email Address*"
                                           autocomplete="email"
                                           required>
                                    <span>Email Address*</span>
                                </label>

                                <label for="confirm_register_email_add"
                                       class="eb_label expanded">
                                    <input type="email"
                                           id="confirm_register_email_add"
                                           aria-label="Confirm Email Address"
                                           placeholder="Confirm Email Address*"
                                           autocomplete="email"
                                           required>
                                    <span>Confirm Email Address*</span>
                                </label>

                                <label for="register_phone_no" class="eb_label expanded">
                                    <input type="tel" name="register_phone_no" id="register_phone_no" aria-label="Phone Number"
                                           placeholder="Phone Number" autocomplete="tel" required>
                                    <span>Phone Number</span>
                                </label>

                                <label for="register_postcode"
                                       class="eb_label expanded">
                                    <input type="text"
                                           name="register_postcode"
                                           id="register_postcode"
                                           aria-label="Postcode lookup"
                                           placeholder="Postcode*"
                                           autocomplete="postal-code" required>
                                    <span>Postcode*</span>
                                    <a href="#" id="postCodeLookup"
                                       class="address_lookup">lookup
                                        address</a>
                                </label>

                            </div>


                        </div>
                        <div class="stage_details_footer">
                            <button type="submit" class="button eb_btn" id="delegateDetailsSubmit1">
                                Create account
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="cell small-12 large-5">
                <form action="" class="second_login">
                    <label class="eb_label">
                        <input type="email" id="logUserEmail" aria-label="Email Address" autocomplete="user-name" placeholder="Email Address" required="" class="error">
                        <span>Email Address</span>
                    </label>
                    <label for="" class="eb_label">
                        <input type="password" placeholder="Password (case sensitive)" aria-label="case sensitive password" autocomplete="current-password" required="" aria-describedby="passwordHelpText" class="error">
                        <span>Password (case sensitive)</span>
                    </label>
                    <p class="help-text" id="passwordHelpText"><a href="#">I've forgotten my
                            password</a></p>
                    <div class="second_login_footer">
                        <button type="submit" class="button eb_btn shrunk">Log in</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<?php include('../template/footer.php'); ?>
