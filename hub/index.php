<?php include('../template/header.php'); ?>

<?php include('../template/search.php'); ?>

    <section class="eb_content_area">

        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                <div class="cell large-10 medium-11 small-12">
                    <div class="breadcrumbs-container">
                        <ol class="breadcrumbs-listing" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="/" role="link" class="breadcrumb-link" title="Home">
                                    <span itemprop="name">Home</span>
                                </a>
                                <meta itemprop="position" content="1">
                            </li>
                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 1">
                                    <span itemprop="name">Breadcrumb link 1</span>
                                </a>
                                <meta itemprop="position" content="2">
                            </li>

                            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="about" role="link" class="breadcrumb-link"
                                   title="Breadcrumb link 2">
                                    <span itemprop="name">Breadcrumb link 2</span>
                                </a>
                                <meta itemprop="position" content="3">
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell large-10 small-12">
                    <div class="eb_headline_block fluid text-center">


                        <h4 class="eb_headline">Resources</h4>
                        <div class="eb_headline_sub">
                            <p>Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero option. EtiamAd usu
                                vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum. Te consul principes
                                ius, harum percipitur intellegebat sea eu, ius ut oratio latine rationibus. In audiam
                                tincidunt mel. Vim ad adhuc augue, eos lorem velit decore in.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-container">
            <div class="grid-x grid-padding-x align-center">
                <div class="cell small-12 large-10">
                    <div class="grid-x grid-padding-x grid-padding-y align-center small-up-1 medium-up-2 large-up-3"
                         data-equalizer data-equalize-by-row="true">
                        <?php for ($i = 0; $i < 3; $i++) : ?>
                            <div class="cell" data-equalizer-watch>
                                <div class="shortcut">
                                    <div class="shortcut_image"
                                         style="background-image: url(//picsum.photos/480/320?image=<?php echo 40 + $i ?>)"></div>
                                    <div class="shortcut_detail">
                                        <div class="title"><img src="/assets/img/pdf_icon.png" alt="">Pdf Name <?php echo $i ?></div>
                                        <div class="excerpt">Download</div>
                                    </div>

                                    <a class="shortcut_link" href="#" aria-label="Course Name <?php echo $i ?>">Course
                                        Name <?php echo $i ?></a>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
                <div class="cell small-12 large-10 text-center">
                    <a href="#" class="button eb_btn shrunk hub">view all</a>
                </div>
            </div>


        </div>
    </section>

    <section class="eb_content_area up-down">
        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-center">
                    <div class="cell large-5 medium-6 small-12">
                        <div class="post_article">
                            <a href="#" title="" class="post_article_thumb"
                               style="background-image: url(//picsum.photos/780/640?image=56)">
                                <span class="text">Blog</span>
                            </a>
                            <div class="post_article_detail">
                                <a href="#" class="title" title="">Read our blog</a>
                                <!-- Limit to maximum of 150 characters -->
                                <div class="excerpt">Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero
                                    option. EtiamAd usu vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum.
                                </div>
                                <a href="#" class="link">view all</a>
                            </div>
                        </div>
                    </div>

                    <div class="cell large-5 medium-6 small-12">
                        <div class="post_article">
                            <a href="#" title="" class="post_article_thumb"
                               style="background-image: url(//picsum.photos/780/640?image=82)">
                                <span class="text">FAQs</span>
                            </a>
                            <div class="post_article_detail">
                                <a href="#" class="title" title="">Frequently Asked Questions</a>
                                <!-- Limit to maximum of 150 characters -->
                                <div class="excerpt">Lorem ipsum dolor sit amet, te sea partiendo suavitate. Ad usu vero
                                    option. EtiamAd usu vero option. Etiam saepe labitur ei mel, ei elit elitr ancillae eum.
                                </div>
                                <a href="#" class="link">view all</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>

<?php include('../template/footer.php'); ?>